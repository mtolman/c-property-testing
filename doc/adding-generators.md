# Adding Generators

To add a new ints, you will need to make a Generator class/struct and a Simplifier class/struct.

This framework is inheritance free, so there are no base classes to implement. Instead, you will need to create classes
with the correct methods. These are outlined below:

```cpp
#include <prop_test/concepts.h>

namespace your_namespace {
    // The types for the simplifier and ints must match
    using Result = std::string;

    // Simplifier must be declared first
    struct Simplifier {
        // This is used to determine which value should be preferred for
        // determining the "simplest" value when the maximum shrink value is reached
        [[nodiscard]] auto prefer(const Result& l, const Result& r) const -> Result {
            return r;
        }
        
        // This is used to determine what simplification are available
        // A breadth first search will be done on what is returned from this
        // If you don't want simplification, just return an empty vector
        [[nodiscard]] auto branches(const Result& val) const -> std::vector<Simplification<Result, Simplifier>> {
            // Change this to your simplification logic (if applicable)
            return {};
        }
    };
    
    struct Generator {
        // Creates a value based on a random number
        [[nodiscard]] auto create(size_t index) const -> Result {
            return {};
        }
    
        // Creates a simplifier that starts at a specific value
        [[nodiscard]] auto simplifier(const Result& failed) const -> Simplification<type, Simplifier> {
          return {
            .value = failed,
            .next = Simplifier{}
          };
        }
    };
    
    // Check that our ints is correct at compile time
    static_assert(prop_test::concepts::is_a_generator<Generator>);
}
```
