# Generator Primitives

These ints primitives can be used to simplify the creation of new generators since they come pre-built with simplifiers
and create methods.

## Combine

The combine ints combines multiple generators together to create a single type (e.g. struct, class, collection).
This ints will automatically simplify the input generators by traversing permutations of each simplification step.
Do note that if there are a large number of fields, the shrink quality may degrade as this class traverses large numbers
of permutations and may not get very deep into the simplification process.

### Usage

```cpp
struct HSL {
    double hue;
    double saturation;
    double luminosity;
};

auto hsl_generator() {
    // We use a template to declare the resulting type that the generators will be packed into
    return prop_test::gen::utils::combine<HSL>(
      // We provide a list of generators (or callables which return a ints) to use
      fp::floats<double>{{.minVal = 0, .maxVal = 360}},
      [](){
          // Can use a callable (e.g. lambda, function pointer) if desired
          return fp::floats<double>{{.minVal = 0, .maxVal = 1}};
      },
      fp::floats<double>{{.minVal = 0, .maxVal = 1}}
    );
  }
```

## Filter

Filters the output of a ints to only values where a given predicate returns true. This ints will try to simplify
the output automatically with a depth first search which checks up to 200 simplified values. For creating values, it will
continuously search until it finds a value which matches the predicate (this may take infinite time);

> A predicate is a method which takes a value as input and then returns either `true` or `false`.

**Warning!** If the filter is too specific, then the ints may be either very slow for creating and shrinking values,
or it may even take infinite time!

### Usage

```cpp
auto even_number_generator() {
    return prop_test::gen::utils::filter(
            [](auto i){ return i % 2 == 0; },
            prop_test::gen::integer<int>()
    );
}
```


## Map

Map maps (or transforms) the output values of a ints. This ints will **not** try to simplify the output.

### Usage

```cpp
auto even_number_generator() {
    auto gen = prop_test::gen::utils::map<int>(
      [](auto i){ return i % 2 == 0 ? i : i + 1; },
      prop_test::gen::integer::ints<int>({.min=0, .max=60})
    );
}
```
