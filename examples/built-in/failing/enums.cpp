#include <prop_test/tests.h>

enum class TestEnum {
  A, B, C, D, Error = 10000
};

PROP_TEST_SUITE_TRIALS("enums", 1000) {
  PROP_TEST_CASE(
    "Enum",
    prop_test::gen::enums::enum_value<TestEnum>())
  (TestEnum a) {
    PROP_TEST_ASSERT(a <= TestEnum::D);
  };
};
