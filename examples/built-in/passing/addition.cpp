#include <prop_test/tests.h>

PROP_TEST_SUITE_TRIALS("addition", 1000) {
  PROP_TEST_CASE(
    "Commutative Property",
    prop_test::gen::integer::ints<int32_t>(),
    prop_test::gen::integer::ints<int32_t>())
  (int32_t a, int32_t b) {
    PROP_TEST_ASSERT((a + b) == (b + a));
  };

  PROP_TEST_CASE(
    "Associative Property",
    prop_test::gen::integer::ints<int32_t>(),
    prop_test::gen::integer::ints<int32_t>(),
    prop_test::gen::integer::ints<int32_t>())
  (int32_t a, int32_t b, int32_t c) {
    PROP_TEST_ASSERT((a + b) + c == a + (b + c));
  };

  PROP_TEST_CASE(
    "Identity Property",
    prop_test::gen::integer::ints<int32_t>())
  (int32_t a) {
    PROP_TEST_ASSERT(a + 0 == a);
  };
};
