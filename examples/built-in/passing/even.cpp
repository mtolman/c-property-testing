#include <prop_test/tests.h>

static auto is_even(int elem) -> bool {
  return elem % 2 == 0;
}

PROP_TEST_SUITE("is_even") {
  PROP_TEST_CASE(
    "With Evens",
    prop_test::gen::utils::filter([](int n){ return n % 2 == 0; }, prop_test::gen::integer::ints<int>())
  ) (int evenNum) {
    PROP_TEST_ASSERT(is_even(evenNum));
  };

  PROP_TEST_CASE(
    "With Odds",
    prop_test::gen::utils::filter([](int n){ return n % 2 != 0; }, prop_test::gen::integer::ints<int>())
      ) (int oddNum) {
    PROP_TEST_ASSERT(!is_even(oddNum));
  };
};
