#include <prop_test/tests.h>

PROP_TEST_SUITE_TRIALS("Faker Generators", 100) {
  PROP_TEST_CASE(
    "Aviation",
    prop_test::gen::faker::aviation::airplanes_generator(),
    prop_test::gen::faker::aviation::airports_generator(),
    prop_test::gen::faker::aviation::airlines_generator(),
    prop_test::gen::faker::aviation::aircraft_types_generator())
  (
    const prop_test::gen::faker::aviation::AviationData& airplaneData,
    const prop_test::gen::faker::aviation::AviationData& airportData,
    const prop_test::gen::faker::aviation::AviationData& airlineData,
    const std::string& type
  ) {
    PROP_TEST_ASSERT(!airplaneData.name.empty());
    PROP_TEST_ASSERT(!airportData.name.empty());
    PROP_TEST_ASSERT(!airlineData.name.empty());
    PROP_TEST_ASSERT(!type.empty());
  };

  PROP_TEST_CASE(
    "Colors",
    prop_test::gen::faker::colors::rgb_generator(),
    prop_test::gen::faker::colors::rgba_generator(),
    prop_test::gen::faker::colors::hsl_generator(),
    prop_test::gen::faker::colors::hsla_generator(),
    prop_test::gen::faker::colors::name_generator())
  (
    const prop_test::gen::faker::colors::RGB& rgb,
    const prop_test::gen::faker::colors::RGBA& rgba,
    const prop_test::gen::faker::colors::HSL& hsl,
    const prop_test::gen::faker::colors::HSLA& hsla,
    const std::string& name
  ) {
    PROP_TEST_ASSERT(!name.empty());
    PROP_TEST_ASSERT(!rgb.css().empty());
    PROP_TEST_ASSERT(!rgba.css().empty());
    PROP_TEST_ASSERT(!hsl.css().empty());
    PROP_TEST_ASSERT(!hsla.css().empty());
  };

  PROP_TEST_CASE(
    "Computers",
    prop_test::gen::faker::computers::cmake_system_names(),
    prop_test::gen::faker::computers::cpu_architectures(),
    prop_test::gen::faker::computers::file_extensions(),
    prop_test::gen::faker::computers::file_mime_mapping(),
    prop_test::gen::faker::computers::file_types(),
    prop_test::gen::faker::computers::mime_types(),
    prop_test::gen::faker::computers::operating_systems()
  )
  (
    const auto& cmake,
    const auto& cpu,
    const auto& ext,
    const auto& mimeMap,
    const auto& fileType,
    const auto& mimeType,
    const auto& os
  ) {
    PROP_TEST_ASSERT(!cmake.empty());
    PROP_TEST_ASSERT(!cpu.empty());
    PROP_TEST_ASSERT(!ext.empty());
    PROP_TEST_ASSERT(!mimeMap.extension.empty());
    PROP_TEST_ASSERT(!fileType.empty());
    PROP_TEST_ASSERT(!mimeType.empty());
    PROP_TEST_ASSERT(!os.empty());
  };

  // We're mostly interested in showing how to use the generators
  // These tests are getting less and less useful since they are heavily tested elsewhere
  // From here on, the examples don't show accessing the parameters
  PROP_TEST_CASE(
    "Finance",
    prop_test::gen::faker::finance::account_number(),
    prop_test::gen::faker::finance::account_name(),
    prop_test::gen::faker::finance::transaction_type(),
    prop_test::gen::faker::finance::amount(),
    prop_test::gen::faker::finance::bic(),
    prop_test::gen::faker::finance::masked_number(),
    prop_test::gen::faker::finance::pin(),
    prop_test::gen::faker::finance::credit_card(),
    prop_test::gen::faker::finance::currency()
  )(
    const auto& acctNum,
    const auto& acctName,
    const auto& txType,
    const auto& amt,
    const auto& bic,
    const auto& mNum,
    const auto& pin,
    const auto& cc,
    const auto& cur
  ) {};

  PROP_TEST_CASE(
    "Internet",
    prop_test::gen::faker::internet::top_level_domain(),
    prop_test::gen::faker::internet::url_schemas(),
    prop_test::gen::faker::internet::ip_v4(),
    prop_test::gen::faker::internet::ip_v6(),
    prop_test::gen::faker::internet::domain()
  )(const auto& ...params) {};

  PROP_TEST_CASE(
    "People",
    prop_test::gen::faker::people::person(),
    prop_test::gen::faker::people::givenName(),
    prop_test::gen::faker::people::surname(),
    prop_test::gen::faker::people::fullName(),
    prop_test::gen::faker::people::prefix(),
    prop_test::gen::faker::people::suffix()
  )(const auto& ...params) {};

  PROP_TEST_CASE(
    "World",
    prop_test::gen::faker::world::country(),
    prop_test::gen::faker::world::timezone()
  )(const auto& ...params) {};
};
