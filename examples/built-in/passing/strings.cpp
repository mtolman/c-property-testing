#include <prop_test/tests.h>

PROP_TEST_SUITE_TRIALS("String Generators", 10) {
  PROP_TEST_CASE(
    "ASCII",
    prop_test::gen::strings::ascii::any(),
    prop_test::gen::strings::ascii::alpha(),
    prop_test::gen::strings::ascii::alpha_lower(),
    prop_test::gen::strings::ascii::alpha_upper(),
    prop_test::gen::strings::ascii::binary(),
    prop_test::gen::strings::ascii::domain_piece(),
    prop_test::gen::strings::ascii::text(),
    prop_test::gen::strings::ascii::math(),
    prop_test::gen::strings::ascii::alpha_numeric(),
    prop_test::gen::strings::ascii::alpha_numeric_lower(),
    prop_test::gen::strings::ascii::alpha_numeric_upper(),
    prop_test::gen::strings::ascii::symbol(),
    prop_test::gen::strings::ascii::html_named(),
    prop_test::gen::strings::ascii::quote(),
    prop_test::gen::strings::ascii::bracket(),
    prop_test::gen::strings::ascii::sentence_punctuation(),
    prop_test::gen::strings::ascii::safe_punctuation(),
    prop_test::gen::strings::ascii::hex(),
    prop_test::gen::strings::ascii::hex_upper(),
    prop_test::gen::strings::ascii::hex_lower(),
    prop_test::gen::strings::ascii::numeric(),
    prop_test::gen::strings::ascii::printable(),
    prop_test::gen::strings::ascii::url_unencoded_chars(),
    prop_test::gen::strings::ascii::url_unencoded_hash_chars(),
    prop_test::gen::strings::ascii::whitespace(),
    prop_test::gen::strings::ascii::control(),
    prop_test::gen::strings::ascii::octal(),
    prop_test::gen::strings::ascii::empty())
  (const auto& ...params){};

  PROP_TEST_CASE(
    "Extended ASCII",
    prop_test::gen::strings::eascii::any(),
    prop_test::gen::strings::eascii::alpha(),
    prop_test::gen::strings::eascii::alpha_lower(),
    prop_test::gen::strings::eascii::alpha_upper(),
    prop_test::gen::strings::eascii::binary(),
    prop_test::gen::strings::eascii::domain_piece(),
    prop_test::gen::strings::eascii::text(),
    prop_test::gen::strings::eascii::math(),
    prop_test::gen::strings::eascii::alpha_numeric(),
    prop_test::gen::strings::eascii::alpha_numeric_lower(),
    prop_test::gen::strings::eascii::alpha_numeric_upper(),
    prop_test::gen::strings::eascii::symbol(),
    prop_test::gen::strings::eascii::html_named(),
    prop_test::gen::strings::eascii::quote(),
    prop_test::gen::strings::eascii::bracket(),
    prop_test::gen::strings::eascii::sentence_punctuation(),
    prop_test::gen::strings::eascii::safe_punctuation(),
    prop_test::gen::strings::eascii::hex(),
    prop_test::gen::strings::eascii::hex_upper(),
    prop_test::gen::strings::eascii::hex_lower(),
    prop_test::gen::strings::eascii::numeric(),
    prop_test::gen::strings::eascii::printable(),
    prop_test::gen::strings::eascii::url_unencoded_chars(),
    prop_test::gen::strings::eascii::url_unencoded_hash_chars(),
    prop_test::gen::strings::eascii::whitespace(),
    prop_test::gen::strings::eascii::control(),
    prop_test::gen::strings::eascii::octal(),
    prop_test::gen::strings::eascii::extended(),
    prop_test::gen::strings::eascii::empty())
  (const auto& ...params){};

  PROP_TEST_CASE(
    "Unicode",
      prop_test::gen::strings::unicode::from_category(
        prop_test::gen::strings::unicode::Category::Ll
      ),
      prop_test::gen::strings::unicode::from_categories({prop_test::gen::strings::unicode::Category::Lu, prop_test::gen::strings::unicode::Category::Ll})
  )
  (const auto& ...params){};
};
