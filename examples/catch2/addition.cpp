#include <catch2/catch_test_macros.hpp>
#include <prop_test/catch2.hpp>

PROP_SUITE("Addition", "[addition]") {
  PROP_TEST_CASE(
    "Commutative Property",
    prop_test::gen::integer::ints<int32_t>(),
    prop_test::gen::integer::ints<int32_t>())
  (int32_t a, int32_t b) {
    PROP_TEST_ASSERT((a + b) == (b + a));
  };

  PROP_TEST_CASE(
    "Associative Property",
    prop_test::gen::integer::ints<int32_t>(),
    prop_test::gen::integer::ints<int32_t>(),
    prop_test::gen::integer::ints<int32_t>())
  (int32_t a, int32_t b, int32_t c) {
    PROP_TEST_ASSERT((a + b) + c == a + (b + c));
  };

  PROP_TEST_CASE(
    "Identity Property",
    prop_test::gen::integer::ints<int32_t>())
  (int32_t a) {
    PROP_TEST_ASSERT(a + 0 == a);
  };
}
