#include <catch2/catch_test_macros.hpp>
#include <prop_test/catch2.hpp>

PROP_SUITE_TRIALS("Subtraction", "[subtraction]", 1000) {
  PROP_TEST_CASE(
    "Identity Property",
    prop_test::gen::integer::ints<int32_t>())
  (int32_t a) {
    PROP_TEST_ASSERT(a - 0 == a);
  };
};
