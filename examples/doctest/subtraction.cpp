#include <doctest/doctest.h>
#include <prop_test/doctest.h>

PROP_SUITE_TRIALS("subtraction", 1000) {
  PROP_TEST_CASE(
    "Identity Property",
    prop_test::gen::integer::ints<int32_t>())
  (int32_t a) {
    PROP_TEST_ASSERT(a - 0 == a);
  };
};
