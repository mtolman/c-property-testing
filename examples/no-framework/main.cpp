#include <prop_test.h>
#include <iostream>

// Helper methods to print our results
void print_failure(const prop_test::FailedCase& failure);
void print_summary(const prop_test::Suite& suite);

int main() {
  auto suite = prop_test::Suite("Example");

  // Define our test cases

  suite.with_generators(
         "Add Example - No Macros",
         prop_test::gen::integer::ints<int>(),
         prop_test::gen::integer::ints<int>())
    .run([](int a, int b) {
      if (a + b != b + a) {
        throw prop_test::AssertFailedError("Commutative property doesn't hold!");
      }
    });

  suite.with_generators(
         "Add Example - Assertion Macros",
         prop_test::gen::integer::ints<int>(),
         prop_test::gen::integer::ints<int>())
    .run([](int a, int b) {
      PROP_TEST_ASSERT(a + b == b + a);
    });

  // Process our output

  print_summary(suite);

  // We get a program exit code we can use
  return suite.program_exit_code();
}

// You may adapt these for your needs (e.g. print in a format that your CI system can parse)
void print_summary(const prop_test::Suite& suite) {
  const auto& summary = suite.result_summary();
  for (const auto& failed : summary.failedCases) {
    print_failure(failed.second);
  }
  for (const auto& errored : summary.erroredCases) {
    print_failure(errored.second);
  }
  std::cout
    << "\n\nPassed: " << summary.passedCases.size()
    << "\nFailed: " << summary.failedCases.size()
    << "\nErrored: " << summary.erroredCases.size() << "\n";
}

void print_failure(const prop_test::FailedCase& failure) {
  std::cout << (failure.error ? "Error: " : "Failed: ")
            << failure.testName
            << "\n"
            << failure.failureMessage
            << "\nShrunk Input: " << failure.simplifiedInputStr
            << "\n\n";
}
