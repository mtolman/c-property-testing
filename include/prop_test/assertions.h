#pragma once

#include <tuple>
#include <string>
#include <functional>
#include <random>
#include <exception>
#include <prop_test/gen/utils/combine.h>
#include <prop_test/shrink.h>
#include <sstream>
#include <map>
#include "common.h"
#include <magic_enum.hpp>

#define PROP_TEST_CAT_IMPL(s1, s2) s1##s2
#define PROP_TEST_CAT(s1, s2)      PROP_TEST_CAT_IMPL(s1, s2)
#ifdef __COUNTER__  // not standard and may be missing for some compilers
#define PROP_TEST_ANONYMOUS(x) PROP_TEST_CAT(x, __COUNTER__)
#else  // __COUNTER__
#define PROP_TEST_ANONYMOUS(x) PROP_TEST_CAT(x, __LINE__)
#endif

namespace prop_test {
  class Suite;

  class AssertFailedError: public std::runtime_error{
  public:
    AssertFailedError(const std::string& msg):runtime_error(msg){}
  };

  struct FailedCase {
    bool error;
    std::string testName;
    std::string failureMessage;
    size_t seed;
    std::string inputStr = {};
    std::string simplifiedInputStr = {};
  };

  struct PassedCase {
    std::string testName;
  };

  namespace impl {

    template <typename T>
    concept Streamable = requires(std::ostream os, T value) {
      { os << value };
    };

    template<typename T>
    struct PrintInput;

    template<Streamable T>
    struct PrintInput<T> {
      static auto print(std::stringstream& ss, const T& t) {
        if constexpr (magic_enum::is_unscoped_enum_v<T> || magic_enum::is_scoped_enum_v<T>) {
          if (magic_enum::enum_contains<T>(t)) {
            ss << magic_enum::enum_type_name<T>() << "::" << magic_enum::enum_name(t);
          }
          else {
            ss << magic_enum::enum_type_name<T>() << "::<INVALID:" << t << ">";
          }
        }
        else {
          ss << t;
        }
      }
    };

    template<typename ...T>
    struct PrintInput<std::tuple<T...>> {
      static auto print(std::stringstream& ss, const std::tuple<T...>& t) {
        ss << "(";
        print_elems(ss, t, std::make_index_sequence<sizeof...(T)>{});
        ss << ")";
      }

      template<size_t ...Index>
      static auto print_elems(std::stringstream& ss, const std::tuple<T...>& t, std::index_sequence<Index...>) {
        const auto _ = (print_elem<Index>(ss, std::get<Index>(t)) | ...);
      }

      template<size_t Index, typename E>
      static auto print_elem(std::stringstream& ss, const E& t) {
        if constexpr (Index != 0) {
          ss << ", ";
        }
        PrintInput<E>::print(ss, t);
        return true;
      }
    };

    template<typename T>
    struct PrintInput<std::vector<T>> {
      static auto print(std::stringstream& ss, const std::vector<T>& t) {
        ss << "[";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "]";
      }
    };

    template<typename T, size_t N>
    struct PrintInput<std::array<T, N>> {
      static auto print(std::stringstream& ss, const std::array<T, N>& t) {
        ss << "[" << N << ":";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "]";
      }
    };

    template<typename T>
    struct PrintInput<std::set<T>> {
      static auto print(std::stringstream& ss, const std::set<T>& t) {
        ss << "set{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename T>
    struct PrintInput<std::multiset<T>> {
      static auto print(std::stringstream& ss, const std::multiset<T>& t) {
        ss << "multiset{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename T>
    struct PrintInput<std::unordered_set<T>> {
      static auto print(std::stringstream& ss, const std::unordered_set<T>& t) {
        ss << "unordered_set{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename T>
    struct PrintInput<std::list<T>> {
      static auto print(std::stringstream& ss, const std::list<T>& t) {
        ss << "list{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "}";
      }
    };

    template<size_t N>
    struct PrintInput<std::bitset<N>> {
      static auto print(std::stringstream& ss, const std::bitset<N>& t) {
        ss << "bitset{" << N << ":" << t.to_string() << "}";
      }
    };

    template<typename K, typename V>
    struct PrintInput<std::map<K, V>> {
      static auto print(std::stringstream& ss, const std::map<K, V>& t) {
        ss << "map{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<K>::print(ss, e.first);
          ss << ": ";
          PrintInput<V>::print(ss, e.second);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename K, typename V>
    struct PrintInput<std::unordered_map<K, V>> {
      static auto print(std::stringstream& ss, const std::unordered_map<K, V>& t) {
        ss << "unordered_map{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<K>::print(ss, e.first);
          ss << ": ";
          PrintInput<V>::print(ss, e.second);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename K, typename V>
    struct PrintInput<std::multimap<K, V>> {
      static auto print(std::stringstream& ss, const std::multimap<K, V>& t) {
        ss << "multimap{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<K>::print(ss, e.first);
          ss << ": ";
          PrintInput<V>::print(ss, e.second);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename T>
    struct PrintInput {
      static auto print(std::stringstream& ss, const T& t) {
        ss << "{?}";
      }
    };

    template<typename ...T>
    auto print_inputs(std::stringstream& ss, const std::tuple<T...>& inputs) {
      PrintInput<std::tuple<T...>>::print(ss, inputs);
    }

    auto get_seed() -> size_t;
    auto set_seed(size_t seed) -> void;

    template<typename ...Generators>
    struct Checker {
      using Gen = gen::utils::impl::combine::Generator<std::tuple<typename GenCreationType<Generators>::type...>, Generators...>;
      Gen generator;
      size_t seed;
      std::mt19937_64 rnd;
      std::string testName;
      std::function<void(PassedCase)> onPass;
      std::function<void(FailedCase)> onFail;
      size_t numTrials;

      auto run(std::function<void (typename GenCreationType<Generators>::type ...)> func) -> bool {
        bool testRun = true;

        std::optional<FailedCase> failure = std::nullopt;

        auto passed = [&func, &testRun, &failure, this](const auto&... params) {
          try {
            func(params...);
            return true;
          }
          catch (const AssertFailedError& e) {
            if (testRun) {
              failure = FailedCase{
                .error = false,
                .testName=testName,
                .failureMessage=e.what(),
                .seed=seed,
              };
            }
            return false;
          }
          catch (const std::runtime_error& e) {
            if (testRun) {
              failure = FailedCase{
                .error = true,
                .testName=testName,
                .failureMessage=e.what(),
                .seed=seed,
              };
            }
            return false;
          }
          catch (const std::string& msg) {
            if (testRun) {
              failure = FailedCase{
                .error = true,
                .testName=testName,
                .failureMessage=msg,
                .seed=seed,
              };
            }
            return false;
          }
          catch (int i) {
            if (testRun) {
              failure = FailedCase{
                .error = true,
                .testName=testName,
                .failureMessage=std::to_string(i),
                .seed=seed,
              };
            }
            return false;
          }
          catch (...) {
            if (testRun) {
              failure = FailedCase{
                .error = true,
                .testName=testName,
                .failureMessage="<UNKNOWN>",
                .seed=seed,
              };
            }
            return false;
          }
        };

        auto call = [&func, &passed, this](const auto& params) {
          return call_func(passed, params, std::make_index_sequence<sizeof...(Generators)>{});
        };

        for (size_t i = 0; i < numTrials; ++i) {
          const auto index = rnd();
          auto val = generator.create(index);
          if (!call(val)) {
            testRun = false;
            auto shrunkInputs = prop_test::shrink(index, generator, call);
            std::stringstream inputStream;
            print_inputs(inputStream, val);
            failure->inputStr = inputStream.str();

            std::stringstream shrunkStream;
            print_inputs(shrunkStream, shrunkInputs);
            failure->simplifiedInputStr = shrunkStream.str();
            onFail(*failure);
            return false;
          }
        }
        onPass({.testName=testName});
        return true;
      }

    private:
      template<typename F, typename ...T, size_t ...Index>
      auto call_func(const F& func, const std::tuple<typename GenCreationType<Generators>::type...>& params, std::index_sequence<Index...>) {
        return func(std::get<Index>(params)...);
      }
    };

    struct SuiteSummary {
      std::string suiteName = {};
      std::vector<std::string> testCases = {};
      std::map<std::string, PassedCase> passedCases = {};
      std::map<std::string, FailedCase> failedCases = {};
      std::map<std::string, FailedCase> erroredCases = {};
    };
  }

  /**
   * Represents a property test suite
   */
  class Suite {
    impl::SuiteSummary summary = {};
    size_t numTrials;
  public:

    /**
     * Creates a new test suite
     * @param name Name of the test suite
     */
    Suite(const std::string& name, size_t numTrials = default_num_iters()) : numTrials(numTrials) {
      summary.suiteName = name;
    }

    /**
     * Will run a test using the specified generators
     * Call `.run` on the result and pass in the function you wish to run
     * Example:
     *
     * ```cpp
     * auto suite = prop_test::Suite("My Test Suite");
     *
     * suite.with_generators("Example Test", prop_test::gen::integer<int>())
     *  .run([](int i) {
     *      // This test will fail, that's okay
     *      PROP_TEST_ASSERT(i % 2 == 1);
     *  });
     * ```
     * @tparam Generators Types of the generators
     * @param testName Name of the test
     * @param generators Tuple of generators to use for the test
     * @return
     */
    template<typename ...Generators>
    auto with_generators(const std::string& testName, std::tuple<Generators...> generators) -> impl::Checker<Generators...> {
      auto seed = impl::get_seed();
      summary.testCases.emplace_back(testName);
      return impl::Checker<Generators...>{
        .generator=combine_impl(generators, std::make_index_sequence<sizeof...(Generators)>{}),
        .seed=impl::get_seed(),
        .rnd=std::mt19937_64(seed),
        .testName = testName,
        .onPass = [this, testName](const PassedCase& passedCase){
          summary.passedCases[testName] = passedCase;
        },
        .onFail = [this, testName](const FailedCase& failedCase){
          if (failedCase.error) {
            summary.erroredCases[testName] = failedCase;
          }
          else {
            summary.failedCases[testName] = failedCase;
          }
        },
        .numTrials = numTrials
      };
    }

    /**
     * Will run a test using the specified generators
     * Call `.run` on the result and pass in the function you wish to run
     * Example:
     *
     * ```cpp
     * auto suite = prop_test::Suite("My Test Suite");
     *
     * suite.with_generators("Example Test", std::make_tuple(prop_test::gen::integer<int>()))
     *  .run([](int i) {
     *      // This test will fail, that's okay
     *      PROP_TEST_ASSERT(i % 2 == 1);
     *  });
     * ```
     * @tparam Generators Types of the generators
     * @param testName Name of the test
     * @param generators Generators to use for the test
     * @return
     */
    template<typename ...Generators>
    auto with_generators(const std::string& testName, Generators... generators) -> impl::Checker<Generators...> {
      return with_generators(testName, std::make_tuple(generators...));
    }

    /**
     * Returns whether the test suite passed
     * @return
     */
    [[nodiscard]] auto did_pass() const -> bool {
      return summary.failedCases.empty() && summary.erroredCases.empty();
    }

    /**
     * Returns the program exit code (to return from main)
     * @return
     */
    [[nodiscard]] auto program_exit_code() const -> int {
      if (did_pass()) {
        return 0;
      }
      return 1;
    }

    /**
     * Prints the results of the test suite
     */
    auto result_summary() const -> const impl::SuiteSummary& {
      return summary;
    }
  private:
    template<typename ...Gens, size_t ...Index>
    static auto combine_impl(const std::tuple<Gens...>& generators, std::index_sequence<Index...>) {
      return gen::utils::combine<std::tuple<typename impl::GenCreationType<Gens>::type...>>(std::get<Index>(generators)...);
    }
  };
}

#define PROP_TEST_ASSERT(COND) if (!(COND)) { auto msg = std::string("Assertion {" + std::string(#COND) + "} failed at ") + std::string(__FILE__) + ":" + std::to_string(__LINE__); throw prop_test::AssertFailedError(msg); }
