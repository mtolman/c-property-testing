#pragma once

#include <iterator>
#include <optional>
#include <tuple>
#include <string>
#include <vector>
#include "common.h"

namespace prop_test {
  /**
   * Represents a simplification step
   * @tparam T
   * @tparam Next
   */
  template<typename T, typename Next>
  struct Simplification {
    /** The simplified value */
    T value;
    /** How to get to the next simplification step */
    Next next;

    /**
     * The possible ways to further simplify the value
     * @return
     */
    auto branches() const { return next.branches(value); }

    /**
     * Compares two values and decides
     * @param l
     * @param r
     * @return
     */
    auto prefer(const T& l, const T& r) const { return next.prefer(l, r); }
  };

  /**
   * @return Default data version
   */
  auto default_version() -> DataVersion;

  /**
   * @ignore
   * @return Default number of iterations for a test suite to run
   */
  auto default_num_iters() -> size_t;
}

namespace prop_test::concepts {
  template<typename T>
  concept ValueSimplifier = requires(const T& g, size_t index) {
    g.simplifier(g.create(index));
  };

  template<typename T>
  concept IndexSimplifier = requires(const T& g, size_t index) {
    g.simplify_index(index);
  };

  /**
   * Concept for determining if a type is a ints
   * @tparam T
   */
  template<typename T>
  concept Generator = requires(const T& g, size_t index) {
    g.create(index);
  } && (ValueSimplifier<T> || IndexSimplifier<T>);

  /**
   * Concept for determining if a type is an enum
   * @tparam T
   */
  template<typename T>
  concept Enum = requires() {
    std::is_enum_v<T> == true;

  };

  /**
   * Concept for determining if a type is weakly comparable (has the "<" operator)
   * @tparam T
   */
  template<typename T>
  concept WeeklyComparable = requires(const T& f, const T& g) {
    { f < g } -> std::convertible_to<bool>;
  };

  template<typename T>
  concept TupleLike =
    requires (T a) {
      std::tuple_size<T>::value;
      std::get<0>(a);
    };

#ifndef PROP_TESTS_CONCEPTS_H_IMPL_IS_A
#define PROP_TESTS_CONCEPTS_H_IMPL_IS_A(Concept) \
    namespace impl { template<typename T> \
    struct IsA ## Concept; \
     \
    template<Concept T> \
    struct IsA ## Concept <T> : std::true_type {}; \
     \
    template<typename T> \
    struct IsA ## Concept : std::false_type {}; }
#endif

    PROP_TESTS_CONCEPTS_H_IMPL_IS_A(Generator)
    PROP_TESTS_CONCEPTS_H_IMPL_IS_A(WeeklyComparable)
    PROP_TESTS_CONCEPTS_H_IMPL_IS_A(TupleLike)
    PROP_TESTS_CONCEPTS_H_IMPL_IS_A(ValueSimplifier)
    PROP_TESTS_CONCEPTS_H_IMPL_IS_A(IndexSimplifier)
#undef PROP_TESTS_CONCEPTS_H_IMPL_IS_A

    /**
     * Constexpr check to see if a type is a ints
     * @tparam T
     */
    template<typename T>
    constexpr auto is_a_generator = impl::IsAGenerator<T>::value;


    template<typename T>
    constexpr auto is_a_value_simplifier = impl::IsAValueSimplifier<T>::value;


    template<typename T>
    constexpr auto is_an_index_simplifier = impl::IsAIndexSimplifier<T>::value;

    /**
     * Constexpr check to see if a type is weakly comparable (has the "<" operator)
     * @tparam T
     */
    template<typename T>
    constexpr auto is_weakly_comparable = impl::IsAWeeklyComparable<T>::value;

    /**
     * Constexpr check to see if a type tuple like
     * @tparam T
     */
    template<typename T>
    constexpr auto is_tuple_like = impl::IsATupleLike<T>::value;

    namespace impl {
      template<typename T, typename = std::void_t<>>
      struct is_std_hashable : std::false_type {};

      template<typename T>
      struct is_std_hashable<T, std::void_t<decltype(std::declval<std::hash<T>>()(std::declval<T>()))>> : std::true_type {};
    }

    /**
     * Constexpr check to see if a type is hashable
     * @tparam T
     */
    template <typename T>
    constexpr bool is_std_hashable_v = impl::is_std_hashable<T>::value;
}
