#pragma once

#include <list>
#include <limits>
#include "prop_test/common.h"
#include <random>

namespace prop_test::gen::collections {
  /**
   * Generates a list of random elements
   *
   * **Warning!** Does not shrink!
   * @tparam Gen Type of ints
   * @param generator Generator for elements
   * @param maxLen Maximum length of the list *Warning!* Making this too big could consume large amounts of elements! Max is 255 by default.
   * @return
   */
  template<typename Gen>
  auto list(const Gen& generator, size_t maxSize = std::numeric_limits<unsigned char>::max());

  namespace impl::list {
    template<typename T>
    struct Simplifier {
      [[nodiscard]] auto prefer(const std::list<T> &l, const std::list<T> &r) const -> const std::list<T>& {
        return l.size() < r.size() ? l : r;
      }
      [[nodiscard]] auto branches(const std::list<T> &val) const -> std::vector<Simplification<std::list<T>, Simplifier>> { return {}; }
    };

    template<typename BaseGenerator>
    struct Generator {
      using T = typename prop_test::impl::GenCreationType<BaseGenerator>::type ;
      BaseGenerator generator;

      size_t maxSize;
      [[nodiscard]] auto create(size_t index) const -> std::list<T> {
        if (maxSize == 0) {
          return {};
        }

        std::mt19937_64 rnd(index);

        auto res = std::list<T>{};
        auto size = rnd() % maxSize;
        for (size_t i = 0; i < size; ++i) {
          res.push_back(generator.create(rnd()));
        }
        return res;
      }

      /**
       * Retrieves a simplifier for a value
       * @param failed Value that needs to be simplified
       * @return
       */
      [[nodiscard]] auto simplifier(const std::list<T> &failed) const -> Simplification<std::list<T>, Simplifier<T>> {
        return Simplification<std::list<T>, Simplifier<T>>{
          .value = failed,
          .next = Simplifier<T>{}};
      }
    };
  }

  template<typename Gen>
  auto list(const Gen& generator, size_t maxSize) {
    return impl::list::Generator<Gen>{.generator=generator, .maxSize = maxSize};
  }
}
