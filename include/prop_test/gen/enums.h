#pragma once

#include "prop_test/gen/utils/map.h"
#include <magic_enum.hpp>
#include <prop_test/core.h>
#include <string>
#include <sstream>

namespace prop_test::gen::enums {
  template<prop_test::concepts::Enum Enum>
  auto enum_value(std::initializer_list<Enum> exclude = {}) {
    if (std::empty(exclude)) {
      return common::no_shrink_generator([exclude](size_t seed) -> Enum {
        const auto v = magic_enum::enum_values<Enum>();
        if (v.size() == 0) {
          return {};
        }

        return v[seed % v.size()];
      });
    }
    else {
      return common::no_shrink_generator([exclude](size_t seed) -> Enum {
        const auto v = magic_enum::enum_values<Enum>();

        if (v.size() == 0) {
          return {};
        }
        if (exclude.size() == v.size()) {
          return {};
        }

        const auto inVec = [&exclude](Enum search) {
          for (const auto& e : exclude) {
            if (e == search) {
              return true;
            }
          }
          return false;
        };

        Enum res;
        do {
          res = v[seed++ % v.size()];
        } while (inVec(res));
        return res;
      });
    }
  }

  template<prop_test::concepts::Enum Enum>
  auto enum_name(std::initializer_list<Enum> exclude = {}) {
    if (std::empty(exclude)) {
      return common::no_shrink_generator([exclude](size_t seed) -> std::string {
        const auto v = magic_enum::enum_names<Enum>();
        if (v.size() == 0) {
          return {};
        }

        const auto res = v[seed % v.size()];
        return std::string(res.begin(), res.end());
      });
    }
    else {
      return common::no_shrink_generator([exclude](size_t seed) -> std::string {
        const auto v = magic_enum::enum_values<Enum>();

        if (v.size() == 0) {
          return {};
        }
        if (exclude.size() == v.size()) {
          return {};
        }

        const auto inVec = [&exclude](Enum search) {
          for (const auto& e : exclude) {
            if (e == search) {
              return true;
            }
          }
          return false;
        };

        Enum res;
        do {
          res = v[seed++ % v.size()];
        } while (inVec(res));

        const auto resName = magic_enum::enum_name<Enum>(res);
        return std::string(resName.begin(), resName.end());
      });
    }
  }

  template<prop_test::concepts::Enum Enum>
  auto typed_enum_name(std::initializer_list<Enum> exclude = {}) {
    return gen::utils::map<std::string>(
      [](const std::string& str) -> std::string {
        std::stringstream ss;
        ss << magic_enum::enum_type_name<Enum>() << "::" << str;
        return ss.str();
      },
      enum_name<Enum>(exclude)
    );
  }
};
