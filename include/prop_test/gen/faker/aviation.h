#pragma once

#include <string>
#include <optional>
#include "common.h"

namespace prop_test::gen::faker::aviation {
  /**
   * Structure containing Aviation specific data
   */
  struct AviationData {
    std::string name;
    std::string iata;
    std::string icao;
  };

  namespace impl {
    template<typename T>
    using IndexGenerator = ::prop_test::gen::faker::impl::IndexGenerator<T>;
    auto aircraft_types(size_t index, DataVersion dataVersion = default_version()) -> std::string;
    auto airlines(size_t index, DataVersion dataVersion = default_version()) -> AviationData;
    auto airplanes(size_t index, DataVersion dataVersion = default_version()) -> AviationData;
    auto airports(size_t index, DataVersion dataVersion = default_version()) -> AviationData;
  }

  /**
   * Returns a ints for aircraft types
   * @param dataVersion Version of faker dataset to use
   * @return Generator<std::string>
   */
  inline auto aircraft_types_generator(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string> { return common::index_generator(impl::aircraft_types, dataVersion); }

  /**
   * Returns a ints for airlines
   * @param dataVersion Version of faker dataset to use
   * @return Generator<AviationData>
   */
  inline auto airlines_generator(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<AviationData> { return common::index_generator(impl::airlines, dataVersion); }

  /**
   * Returns a ints for airplanes
   * @param dataVersion Version of faker dataset to use
   * @return Generator<AviationData>
   */
  inline auto airplanes_generator(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<AviationData> { return common::index_generator(impl::airplanes, dataVersion); }

  /**
   * Returns a ints for airports
   * @param dataVersion Version of faker dataset to use
   * @return Generator<AviationData>
   */
  inline auto airports_generator(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<AviationData> { return common::index_generator(impl::airports, dataVersion); }
}

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::aviation::AviationData& d) -> std::ostream& {
  os << "AviationData{.name=\"" << d.name << "\",.iata=\"" << d.iata << "\",.icao=\"" << d.icao << "\"}";
  return os;
}
