#pragma once

#include "prop_test/gen/faker/common.h"
#include <prop_test/gen/strings/ascii.h>
#include <prop_test/gen/faker/world.h>
#include <prop_test/gen/fp.h>
#include <limits>

namespace prop_test::gen::faker::finance {
  /**
   * Generates an account number of a specific length (e.g. bank account)
   * @param length Length of the account number
   * @return
   */
  inline auto account_number(size_t length = 15) -> strings::ascii::impl::Generator {
    return strings::ascii::numeric({.minLen=length, .maxLen=length});
  }

  /**
   * Generates a random account name (possibly any language)
   * @param maxComplexity Maximum processing complexity for generated names
   * @param dataVersion Faker data version to use
   * @return
   */
  auto account_name(ComplexityOptions options = {}) -> faker::impl::ComplexityGenerator<std::string>;

  /**
   * Generates a random transaction type
   * @param dataVersion Faker data version to use
   * @return
   */
  auto transaction_type(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  struct AmountOptions {
    double max = std::numeric_limits<double>::max();
    double min = 0;
    unsigned decimalPlaces = 2;
    std::string decimalSeparator = ".";
    std::string symbol = {};
    enum SymbolPos {
      PREFIX,
      SUFFIX
    } symbolPos;
  };

  namespace impl {
    struct AmountGenerator;
    struct BicGenerator;
    struct MaskedNumberGenerator;
    struct CreditCardGenerator;
  }

  auto amount(const AmountOptions& options = {}) -> impl::AmountGenerator;

  /**
   * Generates a BIC code
   */
  struct BicOptions {
    /** Whether to include a 3 character branch code */
    bool includeBranchCode = false;
    DataVersion dataVersion = default_version();
  };

  /**
   * Creates a BIC code
   * @param options Bic options
   * @return
   */
  auto bic(BicOptions options = {}) -> impl::BicGenerator;

  /**
   * Options for creating masked numbers (e.g. ####5644, ...4324)
   */
  struct MaskedNumberOptions {
    /** Length of the unmasked portion */
    size_t unMaskedLength = 4;
    /** Length of the masked portion */
    size_t maskedLength = 3;
    /** Character to use for masking */
    char maskedChar = '.';
    /** Whether to surround the text with parenthesis */
    bool surroundWithParens = true;
  };

  /**
   * Generates a masked number
   * @param options Options for masking
   * @return
   */
  auto masked_number(const MaskedNumberOptions& options = {}) -> impl::MaskedNumberGenerator;

  /**
   * Generates a pin
   * @param length Length of the pin
   * @return
   */
  inline auto pin(size_t length = 4) -> strings::ascii::impl::Generator { return strings::ascii::numeric({.minLen=length, .maxLen=length}); }

  /**
   * Represents credit card information
   */
  struct CreditCard {
    /** Card CVV (3 or 4 characters) */
    std::string cvv;
    /** Card Network Id */
    std::string networkId;
    /** Card Network Name */
    std::string networkName;
    /** Card number */
    std::string number;
    /** Card expiration date (format MM/YYYY). May be in the past or future */
    std::string expiration;
  };

  /**
   * Generates a credit card
   * @param dataVersion Data version to use (determines which credit card providers are available)
   * @return
   */
  auto credit_card(DataVersion dataVersion = default_version()) -> impl::CreditCardGenerator;

  /**
   * ISO Currency information
   */
  struct Currency {
    /** ISO currency code */
    std::string code;
    /** ISO currency name */
    std::string name;
    /** Symbol commonly used for the currency */
    std::string symbol;
  };

  /**
   * Generates a credit card
   * @param dataVersion Data version to use (determines which credit card providers are available)
   * @return
   */
  auto currency(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<Currency>;

  namespace impl {
    struct AmountSimplifier {
      AmountOptions options;
      gen::fp::impl::fp::Simplifier<double> simplifier;

      [[nodiscard]] auto prefer(const std::string& l, const std::string& r) const -> std::string;
      [[nodiscard]] auto branches(const std::string& val) const -> std::vector<Simplification<std::string, AmountSimplifier>>;
    };

    struct AmountGenerator {
      AmountOptions options;
      [[nodiscard]] auto create(size_t index) const -> std::string;
      [[nodiscard]] auto simplifier(const std::string& failed) const -> Simplification<std::string, AmountSimplifier>;
    };

    struct BicSimplifier {
      BicOptions options;

      [[nodiscard]] auto prefer(const std::string& l, const std::string& r) const -> std::string { return r; }
      [[nodiscard]] auto branches(const std::string& val) const -> std::vector<Simplification<std::string, BicSimplifier>> { return {}; }
    };

    struct BicGenerator {
      BicOptions options;
      [[nodiscard]] auto create(size_t index) const -> std::string;
      [[nodiscard]] auto simplifier(const std::string& failed) const -> Simplification<std::string, BicSimplifier>;
    };

    struct MaskedNumberSimplifier {
      [[nodiscard]] auto prefer(const std::string& l, const std::string& r) const -> std::string { return r; }
      [[nodiscard]] auto branches(const std::string& val) const -> std::vector<Simplification<std::string, MaskedNumberSimplifier>> { return {}; }
    };

    struct MaskedNumberGenerator {
      MaskedNumberOptions options;
      strings::ascii::impl::Generator baseGenerator;
      [[nodiscard]] auto create(size_t index) const -> std::string;
      [[nodiscard]] auto simplifier(const std::string& failed) const -> Simplification<std::string, MaskedNumberSimplifier>;
    };

    struct CreditCardSimplifier {
      [[nodiscard]] auto prefer(const CreditCard& l, const CreditCard& r) const -> CreditCard { return r; }
      [[nodiscard]] auto branches(const CreditCard& val) const -> std::vector<Simplification<CreditCard, CreditCardSimplifier>> { return {}; }
    };

    struct CreditCardGenerator {
      DataVersion dataVersion;
      [[nodiscard]] auto create(size_t index) const -> CreditCard;
      [[nodiscard]] auto simplifier(const CreditCard& failed) const -> Simplification<CreditCard, CreditCardSimplifier> {
        return {.value=failed, .next=CreditCardSimplifier{}};
      }
    };
  }
}

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::finance::CreditCard& d) -> std::ostream& {
  os << "CreditCard{.cvv=\"" << d.cvv << "\",.number=\"" << d.number << "\",.expiration=\"" << d.expiration << "\",.network=\"" << d.networkId << "\"}";
  return os;
}

inline auto operator<<(std::ostream& os, const prop_test::gen::faker::finance::Currency& d) -> std::ostream& {
  os << "Currency{.name=\"" << d.name << "\",.code=\"" << d.code << "\",.symbol=\"" << d.symbol << "\"}";
  return os;
}
