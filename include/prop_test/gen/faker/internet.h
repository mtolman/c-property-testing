#pragma once

#include "common.h"
#include <string>

namespace prop_test::gen::faker::internet {
  /**
   * Gets a ints for random top level domains
   * @param dataVersion Version of data set to use
   * @return
   */
  auto top_level_domain(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  /**
   * Generator for random URL schemas
   * @param dataVersion Version of data set to use
   * @return
   */
  auto url_schemas(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  /**
   * Generator for IPv4 addresses (e.g. 127.0.0.1)
   * @return
   */
  auto ip_v4() -> prop_test::gen::common::impl::NoShrinkGenerator<std::string>;

  /**
   * Generator for IPv6 addresses (e.g. ::1)
   * @return
   */
  auto ip_v6() -> prop_test::gen::common::impl::NoShrinkGenerator<std::string>;

  /**
   * Generator for random domains
   * @param maxLen Maximum length of the domain (warning: if the top random level domain length is greater than or equal to the max length then this will exceed the max length)
   * @param dataVersion Data set version to use
   * @return
   */
  auto domain(DataVersion dataVersion = default_version()) -> faker::impl::IndexGenerator<std::string>;

  namespace impl::url {
    struct Generator;
  }

  struct UrlOptions {
    enum FLAGS : unsigned {
      NONE = 0x0,
      ALLOW_IPV4_HOST = 0x1 << 0,
      ALLOW_IPV6_HOST = 0x1 << 1,
      ALLOW_ENCODED_CHARS = 0x1 << 2,
      ALLOW_HASH = 0x1 << 3,
      ALLOW_RANDOM_PATH = 0x1 << 4,
      ALLOW_USERNAME = 0x1 << 5,
      ALLOW_PASSWORD = 0x1 << 6,
      ALLOW_QUERY = 0x1 << 7,
      ALLOW_PORT = 0x1 << 8,
    };

    /**
     * Flags used to determine what is allowed to be part of the generated URLs
     * Defaults to "everything is allowed"
     */
    unsigned flags = ~0u;

    /**
     * A set of URL schemas to choose from. Defaults to "any schema from url_schemas for this data set version"
     */
    std::vector<std::string> allowedSchemas = {};
  };

  /**
   * Generator for random URLs
   * @param dataVersion Data set version to use
   * @return
   */
  auto url(const UrlOptions& options = {}, DataVersion dataVersion = default_version()) -> impl::url::Generator;

  namespace impl::url {
    struct Simplifier {
      UrlOptions options;
      DataVersion dataVersion;
      size_t seed;

      [[nodiscard]] auto prefer(const std::string &l, const std::string &r) const -> std::string { return l.size() < r.size() ? l : r; }
      [[nodiscard]] auto branches(const std::string &val) const -> std::vector<Simplification<std::string, Simplifier>>;
    };

    struct Generator {
      using type = std::string;
      UrlOptions options;
      DataVersion dataVersion;
      [[nodiscard]] auto create(size_t index) const -> type;
      [[nodiscard]] auto simplify_index(size_t seed) const -> Simplification<type, Simplifier>;
    };
  }
}
