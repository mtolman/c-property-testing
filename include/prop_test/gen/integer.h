#pragma once

#include <concepts>
#include <prop_test/core.h>

namespace prop_test::gen::integer {

  /**
   * Settings for integer generation
   * @tparam T The integer type to generate
   */
  template<std::integral T>
  struct Settings {
    /**
     * Minimum value for generated integers
     */
    T min = std::numeric_limits<T>::min();

    /**
     * Maximum value for generated integers
     */
    T max = std::numeric_limits<T>::max();
  };

  /**
   * Create a ints for integers
   * @tparam T Type of integer to generate
   * @param minValue Minimum generation value
   * @param maxValue Maximum generation value
   * @return
   */
  template<std::integral T = int>
  auto ints(const Settings<T>& options = {});

  /**
   * Create a ints for integers
   * @tparam T Type of integer to generate
   * @param maxValue Maximum generation value
   * @return
   */
  template<std::integral T = int>
  auto integer(T maxValue = std::numeric_limits<T>::max());

  namespace impl::integer {

    /**
     * Integer simplifier
     * @tparam T The integer type to simplify
     */
    template<std::integral T>
    struct Simplifier {
      /** Integer generation settings */
      Settings<T> settings;

      /**
       * Used to determine which integer to prefer when shrink count is exceeded
       * Does so by magnitude
       * @param l Left param
       * @param r Right param
       * @return
       */
      [[nodiscard]] auto prefer(const T &l, const T &r) const -> T {
        if constexpr (std::is_unsigned_v<T>) {
          return std::min(l, r);
        } else {
          return std::abs(l) < std::abs(r) ? l : r;
        }
      }

      /**
       * Creates simplification branches for a value
       * @param val Value to simplify
       * @return
       */
      [[nodiscard]] auto branches(const T &val) const -> std::vector<Simplification<T, Simplifier>> {
        auto res = std::vector<Simplification<T, Simplifier>>{};
        T up, down;

        // Do a binary search if our range is large
        auto topRange = static_cast<T>(settings.max - val);
        auto bottomRange = static_cast<T>(val - settings.min);

        if constexpr (std::is_signed_v<T>) {
          topRange = std::abs(topRange);
          bottomRange = std::abs(bottomRange);
        }

        if (std::min(topRange, bottomRange) > 50) {
          down = static_cast<T>((val - settings.min) / 2 + settings.min);
          up = static_cast<T>((settings.max - val) / 2 + val);
        } else {
          // Do a single increment/decrement search if our range is small
          down = val - 1;
          up = val + 1;
        }

        auto downAbs = down;
        auto upAbs = up;

        if constexpr (std::is_signed_v<T>) {
          downAbs = std::abs(down);
          upAbs = std::abs(up);
        }

        if (down > settings.min && downAbs <= upAbs) {
          res.emplace_back(Simplification<T, Simplifier>{
            .value = down,
            .next = Simplifier{.settings = {.min = settings.min, .max = static_cast<T>(val - 1)}}});
        }

        if (up < settings.max && downAbs >= upAbs) {
          res.emplace_back(Simplification<T, Simplifier>{
            .value = up,
            .next = Simplifier{.settings = {.min = static_cast<T>(val + 1), .max = settings.max}}});
        }

        res.shrink_to_fit();
        return res;
      }
    };

    /**
     * Integer ints
     * @tparam T Integer type to generate
     */
    template<std::integral T>
    struct Generator {
      using type = T;

      /**
     * Creates a new integer
     * @param index Value to use as a seed for generation
     * @return
     */
      [[nodiscard]] auto create(size_t index) const -> T {
        if (settings.max <= settings.min) {
          return settings.min;
        }
        return static_cast<T>(index % (settings.max - settings.min) + settings.min);
      }

      /**
     * Retrieves a simplifier for a value
     * @param failed Value that needs to be simplified
     * @return
     */
      [[nodiscard]] auto simplifier(const type &failed) const -> Simplification<T, Simplifier<T>> {
        return Simplification<T, Simplifier<T>>{
          .value = failed,
          .next = Simplifier<T>{.settings = settings}};
      }

      Settings<T> settings = {};
    };

    static_assert(prop_test::concepts::is_a_generator<Generator<int>>);
    static_assert(prop_test::concepts::is_a_generator<Generator<int32_t>>);
    static_assert(prop_test::concepts::is_a_generator<Generator<int64_t>>);
    static_assert(prop_test::concepts::is_a_generator<Generator<unsigned int>>);
    static_assert(prop_test::concepts::is_a_generator<Generator<uint32_t>>);
    static_assert(prop_test::concepts::is_a_generator<Generator<uint64_t>>);
  }// namespace impl::integer

  template<std::integral T>
  auto ints(const Settings<T>& options) {
    return impl::integer::Generator<T>{options};
  }
}// namespace prop_test::gen
