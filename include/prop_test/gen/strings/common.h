#pragma once

#include <bitset>
#include <concepts>
#include <limits>
#include <prop_test/common.h>
#include <prop_test/core.h>

namespace prop_test::gen::strings {
  constexpr size_t defaultMaxLen = 1024;
  constexpr size_t defaultMinLen = 1;

  /**
   * Casing to use for alphabetic characters
   */
  enum class Casing {
    UPPER,
    LOWER,
    MIXED
  };

  struct Options {
    size_t minLen = defaultMinLen;
    size_t maxLen = defaultMaxLen;
    DataVersion dataVersion = default_version();
  };
}

namespace prop_test::gen::strings::ranges {
  /**
   * Represents a range of possible characters
   * @tparam Rep Representation of a character
   */
  template<std::integral Rep>
  struct Range {
    using R = Rep;
    Rep begin;
    Rep end;

    [[nodiscard]] size_t size() const {
      if (end <= begin) {
        return 1;
      }
      return end - begin + 1;
    }
  };

  namespace bits {
    /**
     * Struct to determine the number of bit flags needed to filter all possible characters a representation supports
     * @tparam Rep Character representation
     */
    template<typename Rep>
    struct NumBits;

    template<std::integral Rep>
    struct NumBits<Range<Rep>> {
      static constexpr auto value = NumBits<Rep>::value;
    };

    template<std::integral Rep>
    struct NumBits<Rep> {
      static constexpr auto value = static_cast<size_t>(std::numeric_limits<Rep>::max()) + 1;
    };

    /**
     * Returns the number of bits needed to filter all possible characters
     * @tparam R Character representation
     */
    template<typename R>
    constexpr auto num_bits = NumBits<R>::value;

    /**
     * Creates a bitset that represents all characters included in a range of characters
     * On bits are "this character is included", off bits are "this character is excluded"
     *
     * The use of bit sets allows us to use bit operations to combine filters (&, |, ^, ~)
     * @tparam Rep Character representation
     * @tparam bits Bit type
     * @param begin Start of bit range
     * @param end End of bit range
     * @return
     */
    template<std::integral Rep, size_t bits = num_bits<Rep>>
    auto bitset(Rep begin, Rep end) -> std::bitset<bits> {
      std::bitset<bits> res = {};
      for (size_t ch = begin; ch <= end && static_cast<Rep>(ch) >= 0; ++ch) {
        res.set(ch);
      }
      return res;
    }

    /**
     * Creates a bitset that represents all characters included in a range of characters
     * On bits are "this character is included", off bits are "this character is excluded"
     *
     * The use of bit sets allows us to use bit operations to combine filters (&, |, ^, ~)
     * @tparam Rep Character representation
     * @param range Range of bits
     * @return
     */
    template<std::integral Rep>
    auto bitset(const Range<Rep> &range) -> std::bitset<num_bits<Rep>> {
      return bitset(range.begin, range.end);
    }

    /**
     * Creates a bitset that represents a single character being on
     * On bits are "this character is included", off bits are "this character is excluded"
     *
     * The use of bit sets allows us to use bit operations to combine filters (&, |, ^, ~)
     * @tparam Rep Character representation
     * @param value Value of a bit
     * @return
     */
    template<std::integral Rep>
    auto bitset(const Rep &value) -> std::bitset<num_bits<Rep>> {
      return bitset(value, value);
    }

    template<std::integral Rep>
    using bitset_res = std::bitset<num_bits<Rep>>;
  }
}
