#pragma once

#include "common.h"
#include <array>
#include <bitset>
#include <prop_test/core.h>
#include <string>
#include <tuple>
#include <vector>

namespace prop_test::gen::strings::eascii {
  constexpr size_t defaultMaxLen = 1024;
  constexpr size_t defaultMinLen = 1;

  namespace impl {
    struct Generator;
  }

  enum class Casing {
    UPPER,
    LOWER,
    MIXED
  };

  auto extended(Options options = {}) -> impl::Generator;


  /**
   * Returns a ints for strings containing any 7-bit ASCII character
   * @param options Options for generation
   * @return
   */
  auto any(Options options = {}) -> impl::Generator;


  /**
   * Returns a ints for strings containing any alphabetic 7-bit ASCII character
   * @param options Options for generation
   * @return
   */
  auto alpha(Options options = {}) -> impl::Generator;


  /**
   * Returns a ints for strings containing any lowercase alphabetic 7-bit ASCII character
   * @param options Options for generation
   * @return
   */
  auto alpha_lower(Options options = {}) -> impl::Generator;


  /**
   * Returns a ints for strings containing any uppercase alphabetic 7-bit ASCII character
   * @param options Options for generation
   * @return
   */
  auto alpha_upper(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing '1' and '0'
   * @param options Options for generation
   * @return
   */
  auto binary(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing domain characters
   * Does not generate a domain, only a random domain segment (i.e. `aslkidufek` and not `aslkidufek.com`)
   * For getting a full domain, use faker::internet::domain
   * @param options Options for generation
   * @return
   */
  auto domain_piece(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing common text 7-bit ASCII characters (alphabetic, numeric, commas, periods, etc)
   * @param options Options for generation
   * @return
   */
  auto text(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing common math 7-bit ASCII characters (numeric, math operators, etc)
   * @param options Options for generation
   * @return
   */
  auto math(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing alphabetic or numeric 7-bit ASCII characters
   * @param options Options for generation
   * @param casing Casing for alphabetic text
   * @return
   */
  auto alpha_numeric(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing uppercase alphabetic or numeric 7-bit ASCII characters
   * @param options Options for generation
   * @param casing Casing for alphabetic text
   * @return
   */
  auto alpha_numeric_upper(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing lowercase alphabetic or numeric 7-bit ASCII characters
   * @param options Options for generation
   * @param casing Casing for alphabetic text
   * @return
   */
  auto alpha_numeric_lower(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing symbol characters
   * @param maxLen Maximum string length
   * @param casing Casing for alphabetic text
   * @return
   */
  auto symbol(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing characters with named HTML escapes
   * @param maxLen Maximum string length
   * @param casing Casing for alphabetic text
   * @return
   */
  auto html_named(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing quote characters
   * @param maxLen Maximum string length
   * @param casing Casing for alphabetic text
   * @return
   */
  auto quote(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing bracket characters
   * @param maxLen Maximum string length
   * @param casing Casing for alphabetic text
   * @return
   */
  auto bracket(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing sentence punctuation characters
   * @param maxLen Maximum string length
   * @param casing Casing for alphabetic text
   * @return
   */
  auto sentence_punctuation(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing punctuation that's usually safe to process
   * @param maxLen Maximum string length
   * @param casing Casing for alphabetic text
   * @return
   */
  auto safe_punctuation(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for hexadecimal strings
   * @param options Options for generation
   * @param casing Casing for alphabetic text
   * @return
   */
  auto hex(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for uppercase hexadecimal strings
   * @param options Options for generation
   * @param casing Casing for alphabetic text
   * @return
   */
  auto hex_upper(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for lowercase hexadecimal strings
   * @param options Options for generation
   * @param casing Casing for alphabetic text
   * @return
   */
  auto hex_lower(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for numeric strings
   * @param options Options for generation
   * @param casing Casing for alphabetic text
   * @return
   */
  auto numeric(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for strings containing any printable 7-bit ASCII character
   * @param options Options for generation
   * @return
   */
  auto printable(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for unencoded characters for a anywhere in a URL
   * @param options Options for generation
   * @return
   */
  auto url_unencoded_chars(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for unencoded characters for a URL hash section
   * @param options Options for generation
   * @return
   */
  auto url_unencoded_hash_chars(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for whitespace
   * @param options Options for generation
   * @return
   */
  auto whitespace(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for control characters
   * @param options Options for generation
   * @return
   */
  auto control(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for octal characters
   * @param options Options for generation
   * @return
   */
  auto octal(Options options = {}) -> impl::Generator;

  /**
   * Returns a ints for empty text
   * @return
   */
  auto empty() -> impl::Generator;

  namespace impl {
    using Range = prop_test::gen::strings::ranges::Range<uint8_t>;

    struct Settings {
      Options options = {};
      std::bitset<ranges::bits::num_bits<Range>> allowedChars = ranges::bits::bitset(Range{' ', '~'});
    };

    struct Simplifier {
      Settings settings;
      [[nodiscard]] auto prefer(const std::string &l, const std::string &r) const -> std::string { return l.size() < r.size() ? l : r; }
      [[nodiscard]] auto branches(const std::string &val) const -> std::vector<Simplification<std::string, Simplifier>>;
    };

    struct Generator {
      using type = std::string;
      [[nodiscard]] auto create(size_t index) const -> type;
      [[nodiscard]] auto simplifier(const type &failed) const -> Simplification<type, Simplifier>;
      Generator(const Settings &settings = {});

    private:
      void init_cache();
      std::array<char, ranges::bits::num_bits<Range>> cache = {};
      size_t numChars = {};
      Settings settings;
    };

    static_assert(prop_test::concepts::is_a_generator<Generator>);
  }
}
