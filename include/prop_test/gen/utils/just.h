#pragma once

#include <algorithm>
#include <functional>
#include <prop_test/core.h>

namespace prop_test::gen::utils {
  /**
   * Creates a generator that returns just a single value
   * @tparam T Type of value
   * @param value Value return
   * @return
   */
  template<typename T>
  auto just(const T &value);

  namespace impl::just {
    template<typename Result>
    struct Simplifier {

      [[nodiscard]] auto prefer(const Result &l, const Result &r) const -> Result { return r; }

      [[nodiscard]] auto branches(const Result &toSimplify) const -> std::vector<Simplification<Result, Simplifier>> { return {}; }
    };

    template<typename Result>
    struct Generator {
      Result v;

      // Creates a value based on a random number
      [[nodiscard]] auto create(size_t index) const -> Result {
        return v;
      }

      // Creates a simplifier that starts at a specific value
      [[nodiscard]] auto simplify_index(size_t sed) const -> Simplification<Result, Simplifier<Result>> {
        return {
          .value = v,
          .next = Simplifier<Result>{}};
      }
    };
  }// namespace impl::just

  template<typename T>
  auto just(const T &value) {
    return impl::just::Generator<T>{.v = value};
  }
}// namespace prop_test::gen::utils
