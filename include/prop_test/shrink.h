#pragma once

#include "core.h"
#include <queue>
#include <set>
#include <unordered_set>
#include <vector>

namespace prop_test {
  namespace impl::shrink {
    template<typename Generator>
    struct StartSimplify;

    template<concepts::ValueSimplifier Generator>
    struct StartSimplify<Generator> {
      static auto start(const Generator& g, size_t index, const typename GenCreationType<Generator>::type& init) {
        return g.simplifier(init);
      }
    };

    template<concepts::IndexSimplifier Generator>
    struct StartSimplify<Generator> {
      static auto start(const Generator& g, size_t index, const typename GenCreationType<Generator>::type& init) {
        return g.simplify_index(index);
      }
    };

    template<typename Generator>
    auto start_simplify(const Generator& g, size_t index, const typename GenCreationType<Generator>::type& init) {
      return StartSimplify<Generator>::start(g, index, init);
    }
  }

  /**
   * Shrinks a failing input to the "simplest" failing input
   * Simple may mean things for different contexts (e.g. numbers are smallest magnitude, text is shortest length, etc)
   * @tparam Generator Generator type to use to get simplifier instance
   * @tparam Predicate Predicate type which is used to determine if the simplified input still fails or not
   * @tparam MaxShrinks Maximum number of shrink steps to perform
   * @param index The index passed to the ints which created a failing value
   * @param generator The ints which made a failing value
   * @param predicate The predicate to determine if a value succeeds or fails
   * @return The simplest failing input that could be found with the number of steps allocated
   */
  template<concepts::Generator Generator, typename Predicate, size_t MaxShrinks = 8000>
  auto shrink(size_t index, const Generator& generator, const Predicate& predicate) {
    auto lastFailure = generator.create(index);
    if (predicate(lastFailure)) {
      return lastFailure;
    }
    auto start = impl::shrink::start_simplify(generator, index, lastFailure);
    auto toSearch = std::queue<decltype(start)>{};
    toSearch.push(start);

    size_t callCount = 0;

    // If we can use a hash table to track values searched, use that (removes recursive search paths)
    if constexpr (concepts::is_std_hashable_v<decltype(lastFailure)>) {
      auto examined = std::unordered_set< decltype(lastFailure)>{};
      examined.insert(start.value);
      while (toSearch.size() && callCount++ < MaxShrinks) {
        auto simplifier = toSearch.front();
        lastFailure = simplifier.prefer(lastFailure, simplifier.value);
        toSearch.pop();
        auto branches = simplifier.branches();
        for (const auto &branch : branches) {
          if (examined.contains(branch.value)) {
            continue;
          }
          if (!predicate(branch.value)) {
            examined.insert(branch.value);
            toSearch.push(branch);
          }
          else {
            // If we passed, trim down our search to the sibling values (see if we can find something simpler)
            toSearch = {};
            lastFailure = simplifier.value;
            for (const auto&branchToPush : branches) {
              if (!examined.contains(branchToPush.value) && !predicate(branchToPush.value)) {
                toSearch.push(branchToPush);
              }
            }
            break;
          }
        }
      }
      return lastFailure;
    }
    // Otherwise, just keep searching (may result in recursive searches)
    else {
      while (toSearch.size() && callCount++ < MaxShrinks) {
        auto simplifier = toSearch.front();
        lastFailure = simplifier.prefer(lastFailure, simplifier.value);
        toSearch.pop();
        auto branches = simplifier.branches();
        for (const auto &branch : branches) {
          if (!predicate(branch.value)) {
            toSearch.push(branch);
          }
          else {
            // If we passed, trim down our search to the sibling values (see if we can find something simpler)
            toSearch = {};
            lastFailure = simplifier.value;
            for (const auto&branchToPush : branches) {
              if (!predicate(branchToPush.value)) {
                toSearch.push(branchToPush);
              }
            }
            break;
          }
        }
      }
      return lastFailure;
    }
  }
}
