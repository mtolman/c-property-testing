#pragma once

#include <map>
#include <prop_test.h>
#include <functional>

namespace prop_test::impl {
  extern std::map<std::string, std::function<int ()>, std::less<>> test_runners;

  struct RegisterSuite {
    std::string name;
    size_t numTrials = default_num_iters();
  };

  template<typename ...Generators>
  struct RegisterTest {
    std::string name;
    prop_test::Suite& suite;
    std::tuple<Generators...> generators;
  };

  template<typename ...Generators>
  auto register_test(const std::string& name, prop_test::Suite& suite, const std::tuple<Generators...>& generators) {
    return RegisterTest<Generators...>{name, suite, generators};
  }

  auto real_main(int argc, char** argv) -> int;
}

auto operator+(const prop_test::impl::RegisterSuite& registerSuite, std::function<void(prop_test::Suite&)>&& func) -> bool;

template<typename ...Generators>
auto operator+(
  const prop_test::impl::RegisterTest<Generators...>& registerTest,
  const std::function<void (typename prop_test::impl::GenCreationType<Generators>::type...)>& func
) -> bool {
  registerTest.suite.with_generators(registerTest.name, registerTest.generators).run(func);
  return true;
}

#define PROP_TEST_SUITE(NAME) const auto PROP_TEST_ANONYMOUS(suite_def) = prop_test::impl::RegisterSuite{NAME} + [](prop_test::Suite& qqqqqq_suites)
#define PROP_TEST_SUITE_TRIALS(NAME, NUM_TRIALS) const auto PROP_TEST_ANONYMOUS(suite_def) = prop_test::impl::RegisterSuite{NAME, NUM_TRIALS} + [](prop_test::Suite& qqqqqq_suites)
#define PROP_TEST_CASE(TEST_NAME, ...) const auto PROP_TEST_ANONYMOUS(test_def) = prop_test::impl::register_test(TEST_NAME,qqqqqq_suites,std::make_tuple(__VA_ARGS__)) + []

#ifdef PROP_TEST_DEFINE_CUSTOM_MAIN
decltype(prop_test::impl::test_runners) prop_test::impl::test_runners = {};
#endif

#ifdef PROP_TEST_DEFINE_MAIN
decltype(prop_test::impl::test_runners) prop_test::impl::test_runners = {};

int main(int argc, char** argv) {
  return prop_test::impl::real_main(argc, argv);
}
#endif
