#include <prop_test.h>
#include "prop_test/core.h"

static auto get_version() -> prop_test::DataVersion {
  auto *seedCStr = getenv("PROP_TEST_DATA_VERSION");
  size_t version = static_cast<size_t>(prop_test::DataVersion::LATEST);
  if (seedCStr && seedCStr[0] != '\0') {
    version = std::stoull(std::string{seedCStr});
  }
  if (version > static_cast<size_t>(prop_test::DataVersion::LATEST)) {
    return prop_test::DataVersion::LATEST;
  }
  return static_cast<prop_test::DataVersion>(version);
}

auto prop_test::default_version() -> prop_test::DataVersion {
  static const prop_test::DataVersion defaultVersion = get_version();
  return prop_test::DataVersion::LATEST;
}


static auto get_num_version() -> size_t {
  auto *seedCStr = getenv("PROP_TEST_ITERS");
  size_t numIters = 1000;
  if (seedCStr && seedCStr[0] != '\0') {
    numIters = std::stoull(std::string{seedCStr});
    if (numIters == 0) {
      numIters = 500;
    }
  }
  return numIters;
}

auto prop_test::default_num_iters() -> size_t {
  static const size_t iters = get_num_version();
  return iters;
}
