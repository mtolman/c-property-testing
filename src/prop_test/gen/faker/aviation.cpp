#include <prop_test/gen/faker/aviation.h>
#include "common.h"

static auto convert_aviation_types(const FakerDataAviationEntry* v) -> prop_test::gen::faker::aviation::AviationData {
  return {
    .name = v->name,
    .iata = v->iata,
    .icao = v->icao,
  };
}

static const auto aircraft_type_indices_by_version = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_aircraft_types);
auto prop_test::gen::faker::aviation::impl::aircraft_types(size_t index, DataVersion dataVersion) -> std::string {
  return common::at_version_index(index, dataVersion, aircraft_type_indices_by_version)->string;
}

static const auto airlines_indices_by_version = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_airlines);
auto prop_test::gen::faker::aviation::impl::airlines(size_t index, DataVersion dataVersion) -> AviationData {
  return convert_aviation_types(common::at_version_index(index, dataVersion, airlines_indices_by_version));
}

static const auto airplanes_indices_by_version = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_airplanes);
auto prop_test::gen::faker::aviation::impl::airplanes(size_t index, prop_test::gen::faker::DataVersion dataVersion) -> AviationData {
  return convert_aviation_types(common::at_version_index(index, dataVersion, airplanes_indices_by_version));
}

static const auto airports_indices_by_version = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_airports);
auto prop_test::gen::faker::aviation::impl::airports(size_t index, prop_test::gen::faker::DataVersion dataVersion) -> AviationData {
  return convert_aviation_types(common::at_version_index(index, dataVersion, airports_indices_by_version));
}
