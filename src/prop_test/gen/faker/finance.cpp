#include "common.h"
#include <faker-data/faker-data.h>
#include <prop_test/gen/faker/finance.h>
#include <prop_test/gen/utils/filter.h>
#include <sstream>

static const auto account_names_by_complexity = prop_test::gen::faker::common::make_complexity_indices_for_method(faker_data_account_names);

auto prop_test::gen::faker::finance::account_name(prop_test::gen::faker::ComplexityOptions options) -> faker::impl::ComplexityGenerator<std::string> {
  return common::complexity_generator(
    +[](size_t index, DataComplexity complexity, DataVersion dataVersion) -> std::string {
      return common::at_complexity_index(index, dataVersion, complexity, account_names_by_complexity)->text;
    },
    options);
}

static const auto transaction_type_indices = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_transaction_types);

auto prop_test::gen::faker::finance::transaction_type(DataVersion dataVersion) -> faker::impl::IndexGenerator<std::string> {
  return common::index_generator(
    +[](size_t index, DataVersion dataVersion) -> std::string {
      return common::at_version_index(index, dataVersion, transaction_type_indices)->string;
    },
    dataVersion);
}

auto prop_test::gen::faker::finance::amount(const AmountOptions &options) -> impl::AmountGenerator {
  return impl::AmountGenerator{.options = options};
}

auto prop_test::gen::faker::finance::bic(prop_test::gen::faker::finance::BicOptions options) -> prop_test::gen::faker::finance::impl::BicGenerator {
  return prop_test::gen::faker::finance::impl::BicGenerator{
    .options = options};
}
auto prop_test::gen::faker::finance::credit_card(prop_test::gen::faker::DataVersion dataVersion) -> prop_test::gen::faker::finance::impl::CreditCardGenerator {
  return prop_test::gen::faker::finance::impl::CreditCardGenerator{
    .dataVersion = dataVersion};
}
auto prop_test::gen::faker::finance::masked_number(const prop_test::gen::faker::finance::MaskedNumberOptions &options) -> prop_test::gen::faker::finance::impl::MaskedNumberGenerator {
  return prop_test::gen::faker::finance::impl::MaskedNumberGenerator{
    .options = options,
    .baseGenerator = strings::ascii::numeric({.minLen = options.unMaskedLength, .maxLen = options.unMaskedLength})};
}

static const auto currencies = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_currencies);
auto prop_test::gen::faker::finance::currency(prop_test::gen::faker::DataVersion dataVersion) -> prop_test::gen::faker::impl::IndexGenerator<prop_test::gen::faker::finance::Currency> {
  return common::index_generator(
    +[](size_t index, DataVersion dataVersion) -> Currency {
      const auto *c = common::at_version_index(index, dataVersion, currencies);
      return {
        .code = c->isoCode,
        .name = c->name,
        .symbol = c->symbol};
    },
    dataVersion);
}

static auto amount_to_str(const prop_test::gen::faker::finance::AmountOptions &options, double value) {
  const auto fmtStr = "%." + std::to_string(options.decimalPlaces) + "f";
  char *buffer = nullptr;
  asprintf(&buffer, fmtStr.c_str(), value);
  auto res = std::string{buffer};
  free(buffer);

  if (options.decimalSeparator != ".") {
    prop_test::gen::faker::common::replace(res, ".", options.decimalSeparator);
  }

  if (!options.symbol.empty()) {
    if (options.symbolPos == prop_test::gen::faker::finance::AmountOptions::PREFIX) {
      res = options.symbol + res;
    } else {
      res += options.symbol;
    }
  }

  return res;
}

static auto parse_amount(const prop_test::gen::faker::finance::AmountOptions &options, const std::string &val) -> double {
  auto simplifiedVal = val;
  if (!options.symbol.empty()) {
    if (options.symbolPos == prop_test::gen::faker::finance::AmountOptions::PREFIX) {
      simplifiedVal = simplifiedVal.substr(options.symbol.size());
    } else {
      simplifiedVal = simplifiedVal.substr(0, simplifiedVal.size() - options.symbol.size());
    }
  }
  if (options.decimalSeparator != ".") {
    prop_test::gen::faker::common::replace(simplifiedVal, options.decimalSeparator, ".");
  }
  return std::stod(simplifiedVal);
}

auto prop_test::gen::faker::finance::impl::AmountSimplifier::branches(const std::string &val) const -> std::vector<Simplification<std::string, AmountSimplifier>> {
  const auto dVal = parse_amount(options, val);
  auto branches = simplifier.branches(dVal);
  auto convertedBranches = std::vector<Simplification<std::string, AmountSimplifier>>{};
  convertedBranches.reserve(branches.size());
  for (const auto &branch : branches) {
    convertedBranches.emplace_back(Simplification<std::string, AmountSimplifier>{
      .next = AmountSimplifier{.options = options, .simplifier = branch.next},
      .value = amount_to_str(options, branch.value)});
  }
  // Change this to your simplification logic (if applicable)
  return convertedBranches;
}

auto prop_test::gen::faker::finance::impl::AmountSimplifier::prefer(const std::string &l, const std::string &r) const -> std::string {
  return l.size() < r.size() ? l : r;
}

auto prop_test::gen::faker::finance::impl::AmountGenerator::create(size_t index) const -> std::string {
  const std::floating_point auto doubleVal = prop_test::gen::fp::floats<double>({.min = options.min, .max = options.max, .flags = fp::NONE}).create(index);
  return amount_to_str(options, doubleVal);
}

auto prop_test::gen::faker::finance::impl::AmountGenerator::simplifier(const std::string &failed) const -> Simplification<std::string, AmountSimplifier> {
  const auto dVal = parse_amount(options, failed);
  auto dSimplifier = prop_test::gen::fp::floats<double>({.min = options.min, .max = options.max, .flags = fp::NONE}).simplifier(dVal);
  return Simplification<std::string, AmountSimplifier>{
    .value = failed,
    .next = AmountSimplifier{
      .options = options,
      .simplifier = dSimplifier.next}};
}

auto prop_test::gen::faker::finance::impl::BicGenerator::create(size_t index) const -> std::string {
  auto rand = std::mt19937_64(index);
  std::stringstream ss;
  // institution/bank code
  ss << strings::ascii::alpha_upper({4, 4}).create(rand());

  // country code
  ss << prop_test::gen::utils::filter(
          [](const world::Country &country) {
            return !country.alpha2.empty();
          },
          world::country(options.dataVersion)
        )
        .create(rand())
        .alpha2;

  // location code
  ss << strings::ascii::alpha_upper({2, 2}).create(rand());

  if (options.includeBranchCode) {
    ss << strings::ascii::alpha_upper({3, 3}).create(rand());
  }

  return ss.str();
}

auto prop_test::gen::faker::finance::impl::BicGenerator::simplifier(const std::string &failed) const -> prop_test::Simplification<std::string, prop_test::gen::faker::finance::impl::BicSimplifier> {
  return Simplification<std::string, BicSimplifier>{
    .value = failed,
    .next = {}};
}

auto prop_test::gen::faker::finance::impl::MaskedNumberGenerator::create(size_t index) const -> std::string {
  auto v = std::string(options.maskedLength, options.maskedChar) + baseGenerator.create(index);
  if (options.surroundWithParens) {
    v = "(" + v + ")";
  }
  return v;
}

auto prop_test::gen::faker::finance::impl::MaskedNumberGenerator::simplifier(const std::string &failed) const -> prop_test::Simplification<std::string, prop_test::gen::faker::finance::impl::MaskedNumberSimplifier> {
  return prop_test::Simplification<std::string, prop_test::gen::faker::finance::impl::MaskedNumberSimplifier>{
    .value = failed,
    .next = {}};
}

static const auto credit_card_networks = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_cc_networks);

static auto expiration_impl(std::mt19937_64 &rnd) -> std::string {
  std::stringstream ss;
  auto month = std::uniform_int_distribution(1, 12)(rnd);
  auto year = std::uniform_int_distribution(2000, 2400)(rnd);
  constexpr auto lpad = [](std::stringstream &ss, int i) {
    if (i < 10) {
      ss << '0';
    }
    ss << i;
  };
  lpad(ss, month);
  ss << "/" << year;
  return ss.str();
}

static auto card_check_digit(const std::string &cardNumber) -> char {
  size_t sum = 0;
  int multiplier = 2;
  for (auto i = cardNumber.size(); i > 0; --i) {
    const auto index = i - 1;
    sum += (cardNumber[index] - '0') * multiplier;
    // flip between 1 and 2
    multiplier ^= 3;
  }
  return static_cast<char>(9 - (sum + 9) % 10 + '0');
}

static auto card_num_impl(
  prop_test::gen::faker::DataVersion dataVersion,
  const std::string &cardStart,
  size_t len,
  size_t index) -> std::string {
  auto genLen = len - 1 - cardStart.size();
  auto res = cardStart + prop_test::gen::strings::ascii::numeric({genLen, genLen}).create(index);
  return res + card_check_digit(res);
}

auto prop_test::gen::faker::finance::impl::CreditCardGenerator::create(size_t index) const -> CreditCard {
  auto creditCardNetwork = common::at_version_index(index, dataVersion, credit_card_networks);
  auto rnd = std::mt19937_64(index);
  auto len = creditCardNetwork->cardLens[rnd() % creditCardNetwork->numCardLens];
  auto start = creditCardNetwork->cardStarts[rnd() % creditCardNetwork->numCardStarts];
  return CreditCard{
    .cvv = prop_test::gen::strings::ascii::numeric({creditCardNetwork->cvvLen, creditCardNetwork->cvvLen}).create(rnd()),
    .networkId = creditCardNetwork->id,
    .networkName = creditCardNetwork->name,
    .number = card_num_impl(dataVersion, start, len, rnd()),
    .expiration = expiration_impl(rnd),
  };
}
