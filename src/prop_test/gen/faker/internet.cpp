#include "prop_test/gen/faker/internet.h"
#include "common.h"
#include <faker-data/faker-data.h>
#include <prop_test.h>
#include <prop_test/gen/faker/common.h>
#include <sstream>

static auto domain_indexer() {
  size_t numCountries = 0;
  auto countries = faker_data_countries(&numCountries);

  size_t numDomains = 0;
  auto domains = faker_data_top_level_domains(&numDomains);

  auto result = std::array<std::vector<std::string>, static_cast<size_t>(prop_test::gen::faker::DataVersion::LATEST) + 1>{};
  for (size_t version = 0; version < result.size(); ++version) {
    auto domainSet = std::set<std::string>{};
    for (size_t cIndex = 0; cIndex < numCountries; ++cIndex) {
      const FakerDataCountry &country = countries[cIndex];
      if (faker_data_is_active(static_cast<FakerDataVersionNumber>(version), &country.versionInfo) && strlen(country.domain) > 0 && strcmp(country.domain, "-") != 0) {
        // do the `+ 1` to truncate the period
        domainSet.insert(std::string(country.domain + 1));
      }
    }

    for (size_t dIndex = 0; dIndex < numDomains; ++dIndex) {
      const FakerDataStringEntry &domain = domains[dIndex];
      if (faker_data_is_active(static_cast<FakerDataVersionNumber>(version), &domain.versionInfo) && strlen(domain.string) > 0 && strcmp(domain.string, "-") != 0) {
        domainSet.insert(std::string(domain.string));
      }
    }
    auto res = std::vector<std::string>{};
    res.resize(domainSet.size());
    std::copy(domainSet.begin(), domainSet.end(), res.begin());
    result[version] = std::move(res);
  }
  return result;
}

static const auto tlds = domain_indexer();
auto prop_test::gen::faker::internet::top_level_domain(prop_test::gen::faker::DataVersion dataVersion) -> prop_test::gen::faker::impl::IndexGenerator<std::string> {
  return common::index_generator([](size_t index, DataVersion dataVersion) {
    return common::at_version_index(index, dataVersion, tlds);
  });
}

static const auto protocols = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_url_protocols);
auto prop_test::gen::faker::internet::url_schemas(prop_test::gen::faker::DataVersion dataVersion) -> prop_test::gen::faker::impl::IndexGenerator<std::string> {
  return common::index_generator([](size_t index, DataVersion dataVersion) -> std::string {
    return common::at_version_index(index, dataVersion, protocols)->string;
  });
}

auto prop_test::gen::faker::internet::ip_v4() -> prop_test::gen::common::impl::NoShrinkGenerator<std::string> {
  return prop_test::gen::common::no_shrink_generator([](size_t seed) -> std::string {
    std::mt19937_64 rnd(seed);
    auto dst = std::uniform_int_distribution<int>(0, 255);
    auto ss = std::stringstream{};
    ss << dst(rnd) << "." << dst(rnd) << "." << dst(rnd) << "." << dst(rnd);
    return ss.str();
  });
}

auto prop_test::gen::faker::internet::ip_v6() -> prop_test::gen::common::impl::NoShrinkGenerator<std::string> {
  return prop_test::gen::common::no_shrink_generator([](size_t seed) -> std::string {
    std::mt19937_64 rnd(seed);
    bool collapsed = rnd() & 0x100;
    bool ip_v4 = !collapsed && rnd() & 0x100;
    auto dst = std::uniform_int_distribution<uint16_t>(0, std::numeric_limits<uint16_t>::max());
    std::stringstream res{};

    if (ip_v4) {
      res << "::ffff:" << faker::internet::ip_v4().create(seed);
    } else {
      const auto pieces = std::array{
        common::hex_str(dst(rnd)),
        common::hex_str(dst(rnd)),
        common::hex_str(dst(rnd)),
        common::hex_str(dst(rnd)),
        common::hex_str(dst(rnd)),
        common::hex_str(dst(rnd)),
        common::hex_str(dst(rnd)),
        common::hex_str(dst(rnd))};

      if (collapsed) {
        auto num = dst(rnd) % 6 + 2;
        auto offset = dst(rnd) % (8 - num);
        for (size_t i = 0; i < offset; ++i) {
          if (i != 0) {
            res << ":";
          }
          res << pieces[i];
        }

        res << "::";

        for (size_t i = offset + num; i < pieces.size(); ++i) {
          if (i != offset + num) {
            res << ":";
          }
          res << pieces[i];
        }
      } else {
        bool first = true;
        for (const auto &e : pieces) {
          if (!first) {
            res << ":";
          }
          res << e;
          first = false;
        }
      }
    }

    return res.str();
  });
}

auto prop_test::gen::faker::internet::domain(prop_test::gen::faker::DataVersion dataVersion) -> prop_test::gen::faker::impl::IndexGenerator<std::string> {
  static constexpr size_t maxLen = 63;
  return common::index_generator([](size_t index, DataVersion dataVersion) -> std::string {
    std::stringstream ss;
    auto tld = top_level_domain(dataVersion).create(index);
    const int remaining = std::max(1, static_cast<int>(maxLen - tld.size()));
    const size_t maxDomainPieces = std::max(1, remaining / 6);
    const size_t numDomainPieces = std::max(static_cast<size_t>(1), index % maxDomainPieces);
    const size_t maxDomainPieceSize = (remaining - numDomainPieces) / numDomainPieces;
    auto domainPieceGen = gen::strings::ascii::domain_piece({1, maxDomainPieceSize});
    auto rnd = std::mt19937_64(index);

    for (size_t sd = 0; sd < numDomainPieces; ++sd) {
      ss << domainPieceGen.create(rnd()) << ".";
    }
    ss << tld;
    return ss.str();
  }, dataVersion);
}

static void add_url_character(std::ostream& ss, unsigned flags, std::mt19937_64 &rnd) {
  if ((flags & prop_test::gen::faker::internet::UrlOptions::ALLOW_ENCODED_CHARS) != 0 && rnd() & 0x10) {
    ss << "%" << prop_test::gen::strings::ascii::hex({2, 2}).create(rnd());
  }
  else {
    ss << prop_test::gen::strings::ascii::url_unencoded_chars({1, 1}).create(rnd());
  }
}

static auto url_characters(const prop_test::gen::faker::internet::UrlOptions& options, size_t maxSize, size_t seed) {
  auto rnd = std::mt19937_64(seed);
  std::stringstream res;
  size_t size = (rnd() % maxSize) + 1;
  for (size_t i = 0; i < size; ++i) {
    add_url_character(res, options.flags, rnd);
  }
  return res.str();
}

static void add_hash_character(std::ostream& ss, unsigned flags, std::mt19937_64 &rnd) {
  if ((flags & prop_test::gen::faker::internet::UrlOptions::ALLOW_ENCODED_CHARS) != 0 && rnd() & 0x10) {
    ss << "%" << prop_test::gen::strings::ascii::hex({2, 2}).create(rnd());
  }
  else {
    ss << prop_test::gen::strings::ascii::url_unencoded_hash_chars({1, 1}).create(rnd());
  }
}

static auto url_hash_characters(const prop_test::gen::faker::internet::UrlOptions& options, size_t maxSize, size_t seed) {
  auto rnd = std::mt19937_64(seed);
  std::stringstream res;
  size_t size = (rnd() % maxSize) + 1;
  for (size_t i = 0; i < size; ++i) {
    add_hash_character(res, options.flags, rnd);
  }
  return res.str();
}

auto prop_test::gen::faker::internet::url(const UrlOptions& urlOptions, DataVersion dataVersion) -> impl::url::Generator {
  return impl::url::Generator{
    .options = urlOptions,
    .dataVersion = dataVersion
  };
}

static auto add_url_host(std::ostream& ss, const prop_test::gen::faker::internet::UrlOptions& options, prop_test::gen::faker::DataVersion dataVersion, size_t seed) -> void {
  auto rnd = std::mt19937_64(seed);
  auto branch = rnd();
  if (options.flags & prop_test::gen::faker::internet::UrlOptions::ALLOW_IPV4_HOST && branch & 0x00100) {
    ss << prop_test::gen::faker::internet::ip_v4().create(rnd());
  }
  else if (options.flags & prop_test::gen::faker::internet::UrlOptions::ALLOW_IPV6_HOST && branch & 0x01000) {
    ss << "[" << prop_test::gen::faker::internet::ip_v6().create(rnd()) << "]";
  }
  else {
    ss << prop_test::gen::faker::internet::domain(dataVersion).create(rnd());
  }

  if (options.flags & prop_test::gen::faker::internet::UrlOptions::ALLOW_PORT && branch & 0x00400) {
    ss << ":" << static_cast<uint16_t>(rnd());
  }
}

static auto add_url_path(std::ostream& ss, const prop_test::gen::faker::internet::UrlOptions& options, prop_test::gen::faker::DataVersion dataVersion, size_t seed) -> bool {
  using UrlOptions = prop_test::gen::faker::internet::UrlOptions;
  auto rnd = std::mt19937_64(seed);
  if (options.flags & UrlOptions::ALLOW_RANDOM_PATH && rnd() & 0x30) {
    if (rnd() & 0x10) {
      ss << "/";
      return true;
    }

    size_t numSegments = rnd() % 48;

    std::stringstream res;
    res << "/";

    for (size_t i = 0; i < numSegments; ++i) {
      if (i != 0) {
        res << "/";
      }
      url_characters(options, 15, rnd());

      // optional trailing slash
      if (i + 1 == numSegments && (rnd() & 0x1000) != 0) {
        res << "/";
      }
    }
    return true;
  }
  return false;
}

static auto add_url_query(std::ostream& ss, bool& hasPath, const prop_test::gen::faker::internet::UrlOptions& options, prop_test::gen::faker::DataVersion dataVersion, size_t seed) -> void {
  using UrlOptions = prop_test::gen::faker::internet::UrlOptions;
  auto rnd = std::mt19937_64(seed);

  if (!(options.flags & UrlOptions::ALLOW_QUERY) || rnd() & 0x00100) {
    return;
  }

  if (!hasPath) {
    ss << "/";
    hasPath = true;
  }
  ss << "?";
  size_t numPieces = rnd() % 32 + 1;

  for (size_t i = 0; i < numPieces; ++i) {
    if (i != 0) {
      ss << "&";
    }

    ss << url_characters(options, 25, rnd());

    if (rnd() & 0x100) {
      ss << "=";
      ss << url_characters(options, 25, rnd());
    }
  }
}

static auto add_url_hash(std::ostream& ss, bool& hasPath, const prop_test::gen::faker::internet::UrlOptions& options, prop_test::gen::faker::DataVersion dataVersion, size_t seed) -> void {
  using UrlOptions = prop_test::gen::faker::internet::UrlOptions;
  auto rnd = std::mt19937_64(seed);

  if (!(options.flags & UrlOptions::ALLOW_HASH) || rnd() & 0x00100) {
    return;
  }

  if (!hasPath) {
    ss << "/";
    hasPath = true;
  }

  ss << "#";
  ss << url_hash_characters(options, 25, rnd());
}

auto prop_test::gen::faker::internet::impl::url::Generator::create(size_t index) const -> type {
  auto rnd = std::mt19937_64(index);
  std::stringstream ss;

  // We can use `rnd()` here and still be able to safely reduce since both paths call `rnd()`
  if (options.allowedSchemas.empty()) {
    ss << url_schemas(dataVersion).create(rnd());
  }
  else {
    ss << options.allowedSchemas[rnd() % options.allowedSchemas.size()];
  }

  ss << "://";

  auto branches = rnd();
  if (options.flags & UrlOptions::ALLOW_USERNAME && branches & 0x010) {
    ss << url_characters(options, 25, rnd() ^ 0x48271932);
    if (options.flags & UrlOptions::ALLOW_PASSWORD && branches & 0x020) {
      ss << ":" << url_characters(options, 25, rnd() ^ 0x12948302);
    }
    else {
      // Keep our rnd calls balanced
      const auto _ = rnd();
    }
    ss << "@";
  }
  else {
    // Keep our rnd calls balanced
    const auto _ = rnd();
    const auto _1 = rnd();
  }

  add_url_host(ss, options, dataVersion, rnd() ^ 0x847293f3);
  bool hasPath = add_url_path(ss, options, dataVersion, rnd() ^ 0x75129382);
  add_url_query(ss, hasPath, options, dataVersion, rnd() ^ 0x12345678);
  add_url_hash(ss, hasPath, options, dataVersion, rnd() ^ 0x91828374);

  return ss.str();
}

auto prop_test::gen::faker::internet::impl::url::Generator::simplify_index(size_t failedIndex) const -> prop_test::Simplification<Generator::type, Simplifier> {
  return prop_test::Simplification<Generator::type,Simplifier>{
    .value=create(failedIndex),
    .next={
      .options=options,
      .dataVersion=dataVersion,
      .seed=failedIndex
    }
  };
}
auto prop_test::gen::faker::internet::impl::url::Simplifier::branches(const std::string &val) const -> std::vector<Simplification<std::string, Simplifier>> {
  using UrlOptions = prop_test::gen::faker::internet::UrlOptions;
  auto res = std::vector<Simplification<std::string, Simplifier>>{};
#define PROP_TEST_INTERNET_URL_REMOVE_FLAG(FLAG) if (options.flags & (FLAG)) { \
  auto newOpts = options; \
  newOpts.flags ^= (FLAG); \
  res.push_back(Simplification<std::string, Simplifier>{ \
    .value=faker::internet::url(newOpts, dataVersion).create(seed), \
    .next={ \
      .options=newOpts, \
      .dataVersion=dataVersion, \
      .seed=seed \
    } \
  }); \
}

  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_IPV4_HOST)
  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_IPV6_HOST)
  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_ENCODED_CHARS)
  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_HASH)
  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_RANDOM_PATH)
  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_USERNAME)
  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_PASSWORD)
  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_QUERY)
  PROP_TEST_INTERNET_URL_REMOVE_FLAG(UrlOptions::ALLOW_PORT)

#undef PROP_TEST_INTERNET_URL_REMOVE_FLAG
  return res;
}
