#include "prop_test/gen/faker/people.h"
#include "common.h"
#include <faker-data/faker-data.h>
#include <prop_test.h>

template<typename T>
static auto make_sex_indices_for_method(T *(*method)(size_t *outNumEntries)) {
  auto versions = std::array<
    std::array<
      std::array<std::vector<T *>, (static_cast<size_t>(prop_test::gen::faker::people::Sex::FEMALE) + 1)>,
      (static_cast<size_t>(prop_test::gen::faker::DataComplexity::COMPLEX) + 1)>,
    (static_cast<size_t>(prop_test::gen::faker::DataVersion::LATEST) + 1)>{};

  size_t numElems;
  const auto *values = method(&numElems);

  for (size_t i = 0; i < numElems; ++i) {
    for (size_t v = 0; v <= static_cast<size_t>(prop_test::gen::faker::DataVersion::LATEST); ++v) {
      if (faker_data_is_active(static_cast<FakerDataVersionNumber>(v), &values[i].versionInfo)) {
        versions[v][values[i].complexity][values[i].sex].emplace_back(&values[i]);
      }
    }
  }
  return versions;
}

static const auto people_given_names = prop_test::gen::faker::common::make_complexity_indices_for_method(faker_data_given_names);
static const auto people_surnames = prop_test::gen::faker::common::make_complexity_indices_for_method(faker_data_surnames);
static const auto people_prefixes = make_sex_indices_for_method(faker_data_prefixes);
static const auto people_suffixes = prop_test::gen::faker::common::make_complexity_indices_for_method(faker_data_suffixes);

auto prop_test::gen::faker::people::person(prop_test::gen::faker::ComplexityOptions options) -> impl::ComplexityGenerator<Person> {
  return common::complexity_generator([](size_t index, DataComplexity complexity, DataVersion version) -> Person {
    auto givenName = common::at_complexity_index(index, version, complexity, people_given_names);
    auto sex = static_cast<prop_test::gen::faker::people::Sex>(givenName->sex);
    std::optional<std::string> prefix = std::nullopt;
    if (index & 0b00010) {
      const auto &vec = people_prefixes[static_cast<size_t>(version)][static_cast<size_t>(complexity)][static_cast<size_t>(sex)];
      if (!vec.empty()) {
        prefix = vec[index % vec.size()]->name;
      }
    }
    std::optional<std::string> suffix = std::nullopt;
    if (index & 0b00100) {
      const auto &vec = people_suffixes[static_cast<size_t>(version)][static_cast<size_t>(complexity)];
      if (!vec.empty()) {
        suffix = vec[index % vec.size()]->text;
      }
    }
    std::string surname = common::at_complexity_index(index, version, complexity, people_surnames)->text;

    auto fullName = complexity >= DataComplexity::ADVANCED && (index & 0b11000) ? surname + " " + givenName->name : givenName->name + std::string(" ") + surname;

    return {
      .prefix = prefix,
      .suffix = suffix,
      .givenName = givenName->name,
      .surName = surname,
      .fullName = fullName,
      .sex = sex};
  }, options);
}

static auto sex_index_from_seed(size_t seed) -> size_t {
  return seed % 2;
}

auto prop_test::gen::faker::people::prefix(prop_test::gen::faker::ComplexityOptions options) -> prop_test::gen::faker::impl::ComplexityGenerator<std::optional<std::string>> {
  return common::complexity_generator([](size_t index, DataComplexity complexity, DataVersion version) {
    std::optional<std::string> prefix = std::nullopt;
    const auto &vec = people_prefixes[static_cast<size_t>(version)][static_cast<size_t>(complexity)][sex_index_from_seed(index)];
    if (!vec.empty()) {
      prefix = vec[index % vec.size()]->name;
    }
    return prefix;
  }, options);
}

auto prop_test::gen::faker::people::suffix(prop_test::gen::faker::ComplexityOptions options) -> prop_test::gen::faker::impl::ComplexityGenerator<std::optional<std::string>> {
  return common::complexity_generator([](size_t index, DataComplexity complexity, DataVersion version) {
    std::optional<std::string> suffix = std::nullopt;
    const auto &vec = people_suffixes[static_cast<size_t>(version)][static_cast<size_t>(complexity)];
    if (!vec.empty()) {
      suffix = vec[index % vec.size()]->text;
    }
    return suffix;
  }, options);
}

auto prop_test::gen::faker::people::givenName(prop_test::gen::faker::ComplexityOptions options) -> prop_test::gen::faker::impl::ComplexityGenerator<std::string> {
  return common::complexity_generator([](size_t index, DataComplexity complexity, DataVersion version) -> std::string {
    return common::at_complexity_index(index, version, complexity, people_given_names)->name;
  }, options);
}

auto prop_test::gen::faker::people::surname(prop_test::gen::faker::ComplexityOptions options) -> prop_test::gen::faker::impl::ComplexityGenerator<std::string> {
  return common::complexity_generator([](size_t index, DataComplexity complexity, DataVersion version) -> std::string {
    return common::at_complexity_index(index, version, complexity, people_surnames)->text;
  }, options);
}

auto prop_test::gen::faker::people::fullName(prop_test::gen::faker::ComplexityOptions options) -> impl::ComplexityGenerator<std::string> {
  return common::complexity_generator([](size_t index, DataComplexity complexity, DataVersion version) -> std::string {
    auto givenName = common::at_complexity_index(index, version, complexity, people_given_names);
    std::string surname = common::at_complexity_index(index, version, complexity, people_surnames)->text;
    auto fullName = complexity >= DataComplexity::ADVANCED && (index & 0b11000) ? surname + " " + givenName->name : givenName->name + std::string(" ") + surname;
    return fullName;
  }, options);
}
