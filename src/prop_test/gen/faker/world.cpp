#include <prop_test.h>
#include "common.h"
#include "prop_test/gen/faker/world.h"

#include <faker-data/faker-data.h>

static const auto countries_by_index = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_countries);

auto prop_test::gen::faker::world::country(DataVersion dataVersion) -> faker::impl::IndexGenerator<Country> {
  return common::index_generator(
    [](size_t index, DataVersion dataVersion) -> Country {
      const auto* cCountry = common::at_version_index(index, dataVersion, countries_by_index);
      return Country{
        .name = cCountry->name,
        .alpha3 = cCountry->isoAlpha3,
        .alpha2 = cCountry->isoAlpha2,
        .numericCode = cCountry->isoNumericCode,
        .domain = strlen(cCountry->domain) == 0 ? std::nullopt : std::optional(cCountry->domain),
      };
    },
    dataVersion
  );
}

static const auto timezones_by_index = prop_test::gen::faker::common::make_version_indices_for_method(faker_data_timezones);
auto prop_test::gen::faker::world::timezone(prop_test::DataVersion dataVersion) -> prop_test::gen::faker::impl::IndexGenerator<std::string> {
  return common::index_generator([](size_t index, DataVersion version) -> std::string {
    return common::at_version_index(index, version, timezones_by_index)->string;
  }, dataVersion);
}
