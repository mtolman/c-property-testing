#include <prop_test/gen/strings/ascii.h>
#include <random>
#include <unordered_map>
#include "ranges.h"

prop_test::gen::strings::ascii::impl::Generator::Generator(const prop_test::gen::strings::ascii::impl::Settings &settings)
    : settings(settings), cache({}), numChars(0) {
    init_cache();
}

namespace prop_test::gen::strings::ascii::impl {
    struct AsciiGroup {
      const char *name = "";
      std::bitset<128> value;
    };
}

static const auto ascii_groups = [](const prop_test::gen::strings::Options&options) {
  return std::array{
    prop_test::gen::strings::ascii::impl::AsciiGroup{"whitespace", prop_test::gen::strings::ranges::impl::ascii::whitespace(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"control", prop_test::gen::strings::ranges::impl::ascii::control(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"alpha", prop_test::gen::strings::ranges::impl::ascii::alpha(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"hex", prop_test::gen::strings::ranges::impl::ascii::hex(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"numeric", prop_test::gen::strings::ranges::impl::ascii::numeric(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"octal", prop_test::gen::strings::ranges::impl::ascii::octal(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"binary", prop_test::gen::strings::ranges::impl::ascii::binary(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"alphaNumeric", prop_test::gen::strings::ranges::impl::ascii::alphaNumeric(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"printable", prop_test::gen::strings::ranges::impl::ascii::printable(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"symbol", prop_test::gen::strings::ranges::impl::ascii::symbol(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"htmlNamed", prop_test::gen::strings::ranges::impl::ascii::htmlNamed(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"quote", prop_test::gen::strings::ranges::impl::ascii::quote(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"bracket", prop_test::gen::strings::ranges::impl::ascii::bracket(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"punctuation", prop_test::gen::strings::ranges::impl::ascii::sentencePunctuation(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"safe-punctuation", prop_test::gen::strings::ranges::impl::ascii::safePunctuation(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"text", prop_test::gen::strings::ranges::impl::ascii::text(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"math", prop_test::gen::strings::ranges::impl::ascii::math(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"alpha-upper", prop_test::gen::strings::ranges::impl::ascii::alphaUpper(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"alpha-lower", prop_test::gen::strings::ranges::impl::ascii::alphaLower(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"alpha-numeric-upper", prop_test::gen::strings::ranges::impl::ascii::alphaNumericUpper(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"alpha-numeric-lower", prop_test::gen::strings::ranges::impl::ascii::alphaNumericLower(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"hex-upper", prop_test::gen::strings::ranges::impl::ascii::hexUpper(options.dataVersion)},
    prop_test::gen::strings::ascii::impl::AsciiGroup{"hex-lower", prop_test::gen::strings::ranges::impl::ascii::hexLower(options.dataVersion)},
  };
};

static auto add_removed_chars(
  const std::string& str,
  const prop_test::gen::strings::ascii::impl::Settings& settings,
  std::vector<prop_test::Simplification<std::string, prop_test::gen::strings::ascii::impl::Simplifier>>& res
) {
    std::string buffer(str.size() - 1, '\0');
    for (size_t skip = 0; skip < str.size(); ++skip) {
      size_t bufferIndex = 0;
      for (size_t index = 0; index < str.size(); ++index) {
        if (index == skip) {
          continue;
        }
        buffer[bufferIndex++] = str[index];
      }
      res.emplace_back(prop_test::Simplification<std::string, prop_test::gen::strings::ascii::impl::Simplifier>{
        .value=buffer,
        .next={.settings=settings}
      });
    }
}

static auto replace_char_groups(
  const std::string& str,
  const prop_test::gen::strings::ascii::impl::Settings& settings,
  std::vector<prop_test::Simplification<std::string, prop_test::gen::strings::ascii::impl::Simplifier>>& res
) {
    if (settings.allowedChars.none()) {
      return;
    }

    auto copy = str;
    for (const auto& group : ascii_groups(settings.options)) {
      auto combined = settings.allowedChars & group.value;
      if (combined.any()) {
        size_t first = 0;
        for (;first < combined.size(); ++first) {
          if (combined.test(first)) {
            break;
          }
        }
        auto replacementChar = static_cast<char>(first);
        copy = str;
        bool replaced = false;
        for (auto& ch : copy) {
          if (ch >= combined.size() || !combined.test(ch)) {
            ch = replacementChar;
            replaced = true;
          }
        }
        if (replaced) {
          res.emplace_back(prop_test::Simplification<std::string, prop_test::gen::strings::ascii::impl::Simplifier>{
            .value=copy,
            .next={.settings={.options=settings.options, .allowedChars={}}}
          });
        }
      }
    }
}

auto prop_test::gen::strings::ascii::impl::Simplifier::branches(const std::string& str) const -> std::vector<Simplification<std::string, Simplifier>> {
  auto res = std::vector<Simplification<std::string, Simplifier>>{};
  res.reserve(str.size());

  if (str.size() > settings.options.minLen) {
      replace_char_groups(str, settings, res);
      add_removed_chars(str, settings, res);
  }

  res.shrink_to_fit();
  return res;
}

auto prop_test::gen::strings::ascii::impl::Generator::simplifier(const type& failed) const -> Simplification<std::string, Simplifier> {
    return {
      .value = failed,
      .next = Simplifier{.settings=settings}
    };
}

auto prop_test::gen::strings::ascii::impl::Generator::create(size_t index) const -> std::string {
  if (numChars == 0) {
    return "";
  }

  auto rnd = std::mt19937(index);
  // The formula helps us get some nice short inputs, long inputs, and normal-ish input lengths within the first 100 indices
  size_t len = settings.options.maxLen <= settings.options.minLen ? settings.options.minLen : (rnd() * numChars) % (settings.options.maxLen - settings.options.minLen) + settings.options.minLen;

  std::string result(len, '\0');
  if (result.empty()) {
    return result;
  }

  auto dst = std::uniform_int_distribution<size_t>(0, numChars - 1);

  for (char &c : result) {
    c = cache.at(dst(rnd));
  }
  return result;
}

void prop_test::gen::strings::ascii::impl::Generator::init_cache() {
  numChars = settings.allowedChars.count();
  for (size_t arrIndex = 0, bsIndex = 0; bsIndex < 128; ++bsIndex) {
    if (settings.allowedChars[bsIndex]) {
      cache[arrIndex++] = static_cast<char>(bsIndex);
    }
  }
}

auto prop_test::gen::strings::ascii::any(Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::any(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::alpha(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::alpha(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::alpha_lower(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::alphaLower(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::alpha_upper(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::alphaUpper(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::binary(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::binary(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::domain_piece(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::domainPiece(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::text(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::text(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::math(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::math(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::alpha_numeric(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::alphaNumeric(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::alpha_numeric_upper(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::alphaNumericUpper(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::alpha_numeric_lower(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::alphaNumericLower(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::symbol(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::symbol(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::html_named(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::htmlNamed(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::quote(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::quote(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::bracket(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::bracket(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::sentence_punctuation(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::sentencePunctuation(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::safe_punctuation(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::safePunctuation(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::hex(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::hex(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::hex_upper(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::hexUpper(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::hex_lower(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::hexLower(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::numeric(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::numeric(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::printable(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::printable(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::url_unencoded_chars(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::urlUnencodedChars(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::url_unencoded_hash_chars(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::urlUnencodedHashChars(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::whitespace(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::whitespace(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::control(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::control(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::octal(prop_test::gen::strings::Options options) -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({
    .options=options,
    .allowedChars = prop_test::gen::strings::ranges::impl::ascii::octal(options.dataVersion)
  });
}
auto prop_test::gen::strings::ascii::empty() -> prop_test::gen::strings::ascii::impl::Generator {
  return prop_test::gen::strings::ascii::impl::Generator({.options={.minLen = 0, .maxLen = 0}, .allowedChars = {}});
}
