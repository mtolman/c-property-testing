#include "ranges.h"
#include "faker-data/faker-data.h"
#include <array>

template<typename T>
static auto make_ranges_for(const char *key, const FakerDataCharSetGroup *(*method)(size_t *) ) -> std::array<prop_test::gen::strings::ranges::bits::bitset_res<T>, static_cast<size_t>(prop_test::DataVersion::LATEST) + 1> {
  using namespace prop_test::gen::strings::ranges::bits;
  using Res = bitset_res<T>;
  size_t numEntries;
  const auto *groups = method(&numEntries);

  auto results = std::array<Res, static_cast<size_t>(prop_test::DataVersion::LATEST) + 1>{};
  assert(results.size() == static_cast<size_t>(prop_test::DataVersion::LATEST) + 1);

  for (size_t version = 0; version < results.size(); ++version) {
    auto &res = results[version];
    for (size_t index = 0; index < numEntries; ++index) {
      if (strcmp(key, groups[index].name) == 0 && faker_data_is_active(static_cast<FakerDataVersionNumber>(version), &groups[index].versionInfo)) {
        for (size_t rangeIndex = 0; rangeIndex < groups[index].numRanges; ++rangeIndex) {
          const auto &range = groups[index].ranges[rangeIndex];
          res |= bitset<T>(static_cast<T>(range.start), static_cast<T>(range.end));
        }
        break;
      }
    }
  }

  return results;
}

#define PROP_TESTS_RANGES_ASCII_IMPL(KEY, FUNC)                                                                                               \
  static const auto FUNC##Ascii = make_ranges_for<char>(#KEY, faker_data_ascii_character_set_groups);                                         \
  auto prop_test::gen::strings::ranges::impl::ascii::FUNC(DataVersion dataVersion)->prop_test::gen::strings::ranges::bits::bitset_res<char> { \
    return FUNC##Ascii[static_cast<size_t>(dataVersion)];                                                                                     \
  }

#define PROP_TESTS_RANGES_EASCII_IMPL(KEY, FUNC)                                                                                                  \
  static const auto FUNC##EAscii = make_ranges_for<uint8_t>(#KEY, faker_data_extended_ascii_character_set_groups);                                \
  auto prop_test::gen::strings::ranges::impl::eascii::FUNC(DataVersion dataVersion)->prop_test::gen::strings::ranges::bits::bitset_res<uint8_t> { \
    return FUNC##EAscii[static_cast<size_t>(dataVersion)];                                                                                        \
  }

PROP_TESTS_RANGES_ASCII_IMPL(ANY, any)
PROP_TESTS_RANGES_ASCII_IMPL(WHITESPACE, whitespace)
PROP_TESTS_RANGES_ASCII_IMPL(CONTROL, control)
PROP_TESTS_RANGES_ASCII_IMPL(ALPHA_UPPER, alphaUpper)
PROP_TESTS_RANGES_ASCII_IMPL(ALPHA_LOWER, alphaLower)
PROP_TESTS_RANGES_ASCII_IMPL(ALPHA, alpha)
PROP_TESTS_RANGES_ASCII_IMPL(NUMERIC, numeric)
PROP_TESTS_RANGES_ASCII_IMPL(OCTAL, octal)
PROP_TESTS_RANGES_ASCII_IMPL(BINARY, binary)
PROP_TESTS_RANGES_ASCII_IMPL(DOMAIN_PIECE, domainPiece)
PROP_TESTS_RANGES_ASCII_IMPL(URL_UNENCODED_HASH_CHARS, urlUnencodedHashChars)
PROP_TESTS_RANGES_ASCII_IMPL(URL_UNENCODED_CHARS, urlUnencodedChars)
PROP_TESTS_RANGES_ASCII_IMPL(ALPHA_NUMERIC, alphaNumeric)
PROP_TESTS_RANGES_ASCII_IMPL(ALPHA_NUMERIC_UPPER, alphaNumericUpper)
PROP_TESTS_RANGES_ASCII_IMPL(ALPHA_NUMERIC_LOWER, alphaNumericLower)
PROP_TESTS_RANGES_ASCII_IMPL(SYMBOL, symbol)
PROP_TESTS_RANGES_ASCII_IMPL(HTML_NAMED, htmlNamed)
PROP_TESTS_RANGES_ASCII_IMPL(QUOTE, quote)
PROP_TESTS_RANGES_ASCII_IMPL(BRACKET, bracket)
PROP_TESTS_RANGES_ASCII_IMPL(SENTENCE_PUNCTUATION, sentencePunctuation)
PROP_TESTS_RANGES_ASCII_IMPL(SAFE_PUNCTUATION, safePunctuation)
PROP_TESTS_RANGES_ASCII_IMPL(HEX_UPPER, hexUpper)
PROP_TESTS_RANGES_ASCII_IMPL(HEX_LOWER, hexLower)
PROP_TESTS_RANGES_ASCII_IMPL(HEX, hex)
PROP_TESTS_RANGES_ASCII_IMPL(PRINTABLE, printable)
PROP_TESTS_RANGES_ASCII_IMPL(TEXT, text)
PROP_TESTS_RANGES_ASCII_IMPL(MATH, math)

PROP_TESTS_RANGES_EASCII_IMPL(ANY, any)
PROP_TESTS_RANGES_EASCII_IMPL(EXTENDED, extended)
PROP_TESTS_RANGES_EASCII_IMPL(WHITESPACE, whitespace)
PROP_TESTS_RANGES_EASCII_IMPL(CONTROL, control)
PROP_TESTS_RANGES_EASCII_IMPL(ALPHA_UPPER, alphaUpper)
PROP_TESTS_RANGES_EASCII_IMPL(ALPHA_LOWER, alphaLower)
PROP_TESTS_RANGES_EASCII_IMPL(ALPHA, alpha)
PROP_TESTS_RANGES_EASCII_IMPL(NUMERIC, numeric)
PROP_TESTS_RANGES_EASCII_IMPL(OCTAL, octal)
PROP_TESTS_RANGES_EASCII_IMPL(BINARY, binary)
PROP_TESTS_RANGES_EASCII_IMPL(DOMAIN_PIECE, domainPiece)
PROP_TESTS_RANGES_EASCII_IMPL(ALPHA_NUMERIC, alphaNumeric)
PROP_TESTS_RANGES_EASCII_IMPL(ALPHA_NUMERIC_UPPER, alphaNumericUpper)
PROP_TESTS_RANGES_EASCII_IMPL(ALPHA_NUMERIC_LOWER, alphaNumericLower)
PROP_TESTS_RANGES_EASCII_IMPL(URL_UNENCODED_HASH_CHARS, urlUnencodedHashChars)
PROP_TESTS_RANGES_EASCII_IMPL(URL_UNENCODED_CHARS, urlUnencodedChars)
PROP_TESTS_RANGES_EASCII_IMPL(SYMBOL, symbol)
PROP_TESTS_RANGES_EASCII_IMPL(HTML_NAMED, htmlNamed)
PROP_TESTS_RANGES_EASCII_IMPL(QUOTE, quote)
PROP_TESTS_RANGES_EASCII_IMPL(BRACKET, bracket)
PROP_TESTS_RANGES_EASCII_IMPL(SENTENCE_PUNCTUATION, sentencePunctuation)
PROP_TESTS_RANGES_EASCII_IMPL(SAFE_PUNCTUATION, safePunctuation)
PROP_TESTS_RANGES_EASCII_IMPL(HEX_UPPER, hexUpper)
PROP_TESTS_RANGES_EASCII_IMPL(HEX_LOWER, hexLower)
PROP_TESTS_RANGES_EASCII_IMPL(HEX, hex)
PROP_TESTS_RANGES_EASCII_IMPL(PRINTABLE, printable)
PROP_TESTS_RANGES_EASCII_IMPL(TEXT, text)
PROP_TESTS_RANGES_EASCII_IMPL(MATH, math)

#undef PROP_TESTS_RANGES_ASCII_IMPL
#undef PROP_TESTS_RANGES_EASCII_IMPL

static auto make_unicode_category_ranges() {
  using namespace prop_test::gen::strings::ranges;
  size_t numCategories;
  const auto *categories = faker_data_unicode_character_categories(&numCategories);
  auto range = std::array<
    std::array<
      std::vector<Range<uint32_t>>,
      static_cast<size_t>(prop_test::gen::strings::unicode::Category::Zs) + 1>,
    static_cast<size_t>(prop_test::DataVersion::LATEST) + 1
  >{};

  for (size_t version = 0; version <= static_cast<size_t>(prop_test::DataVersion::LATEST); ++version) {
    for (size_t cat = 0; cat < numCategories; ++cat) {
      const auto &category = categories[cat];
      if (faker_data_is_active(static_cast<FakerDataVersionNumber>(version), &category.versionInfo)) {
        for (size_t i = 0; i < category.numRanges; ++i) {
          range[static_cast<size_t>(version)][static_cast<size_t>(category.category)].emplace_back(Range<uint32_t>{
            .begin = category.ranges[i].start,
            .end = category.ranges[i].end});
        }
      }
    }
  }
  return range;
}
static const auto unicode_category_ranges = make_unicode_category_ranges();

auto prop_test::gen::strings::ranges::impl::unicode::category_ranges(prop_test::gen::strings::unicode::Category category, DataVersion dataVersion) -> std::vector<ranges::Range<uint32_t>> {
  return unicode_category_ranges[static_cast<size_t>(dataVersion)][static_cast<size_t>(category)];
}
