#pragma once

#include <prop_test/gen/strings/common.h>
#include <prop_test/gen/strings/unicode.h>
#include <prop_test/common.h>
#include <vector>

namespace prop_test::gen::strings::ranges::impl {
  namespace ascii {
    auto any(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto whitespace(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto control(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto alphaUpper(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto alphaLower(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto alpha(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto numeric(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto octal(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto binary(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto domainPiece(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto alphaNumeric(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto alphaNumericUpper(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto alphaNumericLower(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto symbol(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto htmlNamed(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto quote(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto bracket(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto sentencePunctuation(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto safePunctuation(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto hexUpper(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto hexLower(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto hex(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto printable(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto text(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto math(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto urlUnencodedChars(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;

    auto urlUnencodedHashChars(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<char>;
  }

  namespace eascii {
    auto any(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto whitespace(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto control(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto alphaUpper(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto alphaLower(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto alpha(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto numeric(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto octal(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto binary(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto domainPiece(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto alphaNumeric(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto alphaNumericUpper(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto alphaNumericLower(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto symbol(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto htmlNamed(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto quote(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto bracket(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto sentencePunctuation(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto safePunctuation(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto hexUpper(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto hexLower(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto hex(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto printable(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto text(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto math(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto urlUnencodedChars(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto urlUnencodedHashChars(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;

    auto extended(DataVersion dataVersion = default_version()) -> prop_test::gen::strings::ranges::bits::bitset_res<uint8_t>;
  }

  namespace unicode {
    auto category_ranges(prop_test::gen::strings::unicode::Category category, DataVersion dataVersion = default_version()) -> std::vector<ranges::Range<uint32_t>>;
  }
}
