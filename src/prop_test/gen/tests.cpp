#include <prop_test/tests.h>
#include <docopt.h>
#include <sstream>
#include <set>
#include <iostream>

constexpr auto summaryDivider = "==============================================\n";
constexpr auto caseDivider = "----------------------------------------------\n";

auto operator+(const prop_test::impl::RegisterSuite& registerSuite, std::function<void(prop_test::Suite&)>&& func) -> bool {
  prop_test::impl::test_runners.insert_or_assign(
    registerSuite.name,
    [
      f = std::move(func),
     name = registerSuite.name,
     trials = registerSuite.numTrials
  ]() -> int {
      prop_test::Suite suite(name, trials);
      f(suite);
      const auto& summary = suite.result_summary();

      std::cout << "\n" << summaryDivider << "Suite: " << name << "\n";

      for (const auto& testCase : summary.testCases) {
        const auto passIt = summary.passedCases.find(testCase);
        const auto errorIt = summary.erroredCases.find(testCase);
        const auto failedIt = summary.failedCases.find(testCase);
        std::cout << caseDivider;

        if (passIt != summary.passedCases.end()) {
          std::cout << "Pass: " << passIt->second.testName << "\n";
        }
        else if (errorIt != summary.erroredCases.end()) {
          std::cout << "ERROR: " << errorIt->second.testName << "\n"
                    << "ErrMsg: " << errorIt->second.failureMessage << "\n"
                    << "Seed: " << errorIt->second.seed << "\n"
                    << "Input: " << errorIt->second.inputStr << "\n"
                    << "Simplified: " << errorIt->second.simplifiedInputStr << "\n";
        }
        else if (failedIt != summary.failedCases.end()) {
          std::cout << "FAILURE: " << errorIt->second.testName << "\n"
                    << "FailMsg: " << errorIt->second.failureMessage << "\n"
                    << "Seed: " << errorIt->second.seed << "\n"
                    << "Input: " << errorIt->second.inputStr << "\n"
                    << "Simplified: " << errorIt->second.simplifiedInputStr << "\n";
        }
      }

      std::cout << caseDivider
                << "Passed: " << summary.passedCases.size()
                << "\tError: " << summary.erroredCases.size()
                << "\tFailed: " << summary.failedCases.size() << "\n"
                << summaryDivider;

      return suite.program_exit_code();
    });
  return true;
}

auto prop_test::impl::real_main(int argc, char **argv) -> int {
  std::string usage =
    R"(Property Testing

  Usage:
    )" + std::string(argv[0]) + R"( [--test-suites=<testsuites>] [--seed=<seed>]
    )" + std::string(argv[0]) + R"( (-h | --help)

  Options:
    -h --help                   Show this screen.
    --test-suites=<testsuites>  Comma separated list of test suites to run.
                                Will run all test suites if not provided
    --seed=<seed>               Random number seed to use.
                                Can also be set with the environment variable PROP_TEST_SEED.
                                Will be automatically selected if not provided.
)";

  std::map<std::string, docopt::value> args = docopt::docopt(usage, {argv + 1, argv + argc}, true);

  if (args["--help"].asBool()) {
    return 0;
  }

  if (args["--seed"]) {
    set_seed(std::stoull(args["--seed"].asString()));
  }

  std::set<std::string> suites = {};

  if (args["--test-suites"]) {
    auto inputSuites = std::stringstream(args["--test-suites"].asString());
    std::string suite;
    while (getline(inputSuites, suite, ',')) {
      suites.insert(suite);
    }
  }

  int returnCode = 0;
  for (const auto& [suite_name, runner] : prop_test::impl::test_runners) {
    if (suites.empty() || suites.contains(suite_name)) {
      returnCode |= runner();
    }
    else {
      std::cout << "\n" << summaryDivider
                << "Suite: " << suite_name << "\n"
                << caseDivider
                << "SKIPPED\n" << summaryDivider;
    }
  }

  return returnCode;
}
