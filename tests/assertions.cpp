#include <doctest/doctest.h>
#include <prop_test.h>

TEST_CASE("Assertions") {
  size_t numTrials = 400;
  auto suite = prop_test::Suite("Assertions", numTrials);

  size_t numRuns = 0;
  CHECK(suite.with_generators("No Asserts", prop_test::gen::integer::ints<int>({.min = 0, .max = 1000}))
    .run([&numRuns](int i) {
      ++numRuns;
    }));

  CHECK_EQ(numRuns, numTrials);
  CHECK(suite.did_pass());
  CHECK_EQ(suite.program_exit_code(), 0);

  CHECK(suite.with_generators("Passing Asserts", prop_test::gen::integer::ints<int>({.min = 0, .max = 1000}))
    .run([](int i) {
      PROP_TEST_ASSERT(i >= 0);
    }));

  CHECK(suite.did_pass());
  CHECK_EQ(suite.program_exit_code(), 0);

  CHECK_FALSE(suite.with_generators("Failing Asserts", prop_test::gen::integer::ints<int>({.min = 0, .max = 1000}))
                .run([](int i) {
                  PROP_TEST_ASSERT(i < 0);
                }));

  CHECK_FALSE(suite.did_pass());
  CHECK_EQ(suite.program_exit_code(), 1);

  CHECK_FALSE(suite.with_generators("error", prop_test::gen::integer::ints<int>({.min = 0, .max = 1000}))
                .run([](int i) {
                  throw std::runtime_error("Test error");
                }));
}
