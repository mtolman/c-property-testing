#include <prop_test.h>
#include <doctest/doctest.h>

TEST_SUITE("Map") {
  TEST_CASE("Generation") {
    auto gen = prop_test::gen::collections::map(
      prop_test::gen::integer::ints<unsigned>({.min = 0, .max = 100}),
      prop_test::gen::integer::ints<int64_t>({.min = 200, .max = 300})
    );
    for (size_t i = 0; i < 100; ++i) {
      auto val = gen.create(i);
      for (const auto& e : val) {
        CHECK_GE(e.first, 0);
        CHECK_LE(e.first, 100);
        CHECK_GE(e.second, 200);
        CHECK_LE(e.second, 300);
      }
    }
  }

  TEST_CASE("Shrinkage") {
    auto gen = prop_test::gen::collections::map(
      prop_test::gen::integer::ints({.min = 0, .max = 100}),
      prop_test::gen::integer::ints({.min = 200, .max = 300})
    );
    auto pred = [](const auto& v) { return v.size() < 10; };
    const auto start = 9;
    REQUIRE_FALSE_MESSAGE(pred(gen.create(start)), gen.create(start).size());
    auto shrunk = prop_test::shrink(start, gen, pred);
    // Shouldn't shrink
    CHECK_EQ(shrunk.size(), gen.create(start).size());
  }
}
