#include <prop_test.h>
#include <doctest/doctest.h>

TEST_SUITE("Tuple") {
  TEST_CASE("Generation") {
    auto gen = prop_test::gen::collections::tuple(
      prop_test::gen::integer::ints({.min = 0, .max = 100}),
      prop_test::gen::integer::ints<int64_t>({.min = 200, .max = 300})
    );
    for (size_t i = 0; i < 100; ++i) {
      auto val = gen.create(i);
      CHECK_GE(std::get<0>(val), 0);
      CHECK_LE(std::get<0>(val), 100);
      CHECK_GE(std::get<1>(val), 200);
      CHECK_LE(std::get<1>(val), 300);
    }
  }
}
