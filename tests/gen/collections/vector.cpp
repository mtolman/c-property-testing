#include <prop_test.h>
#include <doctest/doctest.h>

TEST_SUITE("Vector") {
  TEST_CASE("Generation") {
    auto gen = prop_test::gen::collections::vector(prop_test::gen::integer::ints({.min = 0, .max = 100}));
    for (size_t i = 0; i < 100; ++i) {
      auto val = gen.create(i);
      for (const auto& e : val) {
        CHECK_GE(e, 0);
        CHECK_LE(e, 100);
      }
    }
  }

  TEST_CASE("Shrinkage") {
    auto gen = prop_test::gen::collections::vector(prop_test::gen::integer::ints({.min = 0, .max = 100}));
    auto pred = [](const std::vector<int>& v) { return v.size() < 30; };
    const auto start = 9;
    REQUIRE_FALSE_MESSAGE(pred(gen.create(start)), gen.create(start).size());
    auto shrunk = prop_test::shrink(start, gen, pred);
    CHECK_LT(shrunk.size(), gen.create(start).size());
  }
}
