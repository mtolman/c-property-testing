#include <doctest/doctest.h>
#include <prop_test.h>
#include <regex>


TEST_SUITE("Computers") {
  TEST_CASE("CMake System Names") {
    auto generator = prop_test::gen::faker::computers::cmake_system_names();
    auto regex = std::regex("(Windows|Linux|Darwin)");
    for (size_t i = 0; i < 10; ++i) {
      CHECK_MESSAGE(std::regex_match(generator.create(i), regex), generator.create(i));
    }
  }

  TEST_CASE("CPU Architectures") {
    auto generator = prop_test::gen::faker::computers::cpu_architectures();
    auto regex = std::regex("[\\w\\-]+");
    for (size_t i = 0; i < 10; ++i) {
      CHECK_MESSAGE(std::regex_match(generator.create(i), regex), generator.create(i));
    }
  }

  TEST_CASE("File Extensions") {
    auto generator = prop_test::gen::faker::computers::file_extensions();
    auto regex = std::regex("[^.]+");
    for (size_t i = 0; i < 10; ++i) {
      CHECK_MESSAGE(std::regex_match(generator.create(i), regex), generator.create(i));
    }
  }

  TEST_CASE("File Mime Mappings") {
    auto generator = prop_test::gen::faker::computers::file_mime_mapping();
    auto regex = std::regex("[^.]+");
    for (size_t i = 0; i < 10; ++i) {
      CHECK_MESSAGE(std::regex_match(generator.create(i).extension, regex), generator.create(i));
    }
  }

  TEST_CASE("File Types") {
    auto generator = prop_test::gen::faker::computers::file_types();
    auto regex = std::regex("\\w+");
    for (size_t i = 0; i < 10; ++i) {
      CHECK_MESSAGE(std::regex_match(generator.create(i), regex), generator.create(i));
    }
  }

  TEST_CASE("File Types") {
    auto generator = prop_test::gen::faker::computers::mime_types();
    auto regex = std::regex(R"([\w\-+.]+\/[\w\-+.]+)");
    for (size_t i = 0; i < 10; ++i) {
      CHECK_MESSAGE(std::regex_match(generator.create(i), regex), generator.create(i));
    }
  }

  TEST_CASE("Operating Systems") {
    auto generator = prop_test::gen::faker::computers::operating_systems();
    auto regex = std::regex("\\w+");
    for (size_t i = 0; i < 10; ++i) {
      CHECK_MESSAGE(std::regex_match(generator.create(i), regex), generator.create(i));
    }
  }
}