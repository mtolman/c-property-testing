#include <doctest/doctest.h>
#include <prop_test.h>

TEST_SUITE("World") {
  TEST_CASE("Countries") {
    auto gen = prop_test::gen::faker::world::country();
    for (size_t i = 0; i < 1000; ++i) {
      auto p = gen.create(i);
      CHECK_GT(p.name.size(), 0);
    }
  }
}
