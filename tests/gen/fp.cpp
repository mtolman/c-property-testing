#include <cmath>
#include <doctest/doctest.h>
#include <prop_test.h>

#define FP_CASE(Type, Min, Max)                                    \
  SUBCASE("Min: " #Min ", Max: " #Max) {                           \
    auto gen = prop_test::gen::fp::floats<Type>({.min = Min, .max = Max}); \
    for (size_t i = 0; i < 100; ++i) {                             \
      const std::floating_point auto v = gen.create(i);            \
      CHECK_GE(v, Min - std::numeric_limits<Type>::epsilon());     \
      CHECK_LE(v, Max + std::numeric_limits<Type>::epsilon());     \
    }                                                              \
  }

#define INF_CASE(Type)                                                                                       \
  SUBCASE("Infinity") {                                                                                      \
    auto gen = prop_test::gen::fp::floats<Type>({.min = 0, .max = 100, .flags = prop_test::gen::fp::ALLOW_INFINITY}); \
    bool foundInf = false;                                                                                   \
    bool foundNegInf = false;                                                                                \
    for (size_t i = 200; i < 1000; ++i) {                                                                    \
      const std::floating_point auto v = gen.create(i);                                                      \
      if (v == std::numeric_limits<Type>::infinity()) {                                                      \
        foundInf = true;                                                                                     \
      }                                                                                                      \
      if (v == std::numeric_limits<Type>::infinity()) {                                                      \
        foundNegInf = true;                                                                                  \
      }                                                                                                      \
    }                                                                                                        \
    CHECK_MESSAGE(foundInf, "Did not find infinity");                                                        \
    CHECK_MESSAGE(foundNegInf, "Did not find negative infinity");                                            \
  }

#define NAN_CASE(Type)                                                                                  \
  SUBCASE("NaN") {                                                                                      \
    auto gen = prop_test::gen::fp::floats<Type>({.min = 0, .max = 100, .flags = prop_test::gen::fp::ALLOW_NAN}); \
    bool foundNan = false;                                                                              \
    for (size_t i = 300; i < 1000; ++i) {                                                               \
      if (std::isnan(gen.create(i))) {                                                                  \
        foundNan = true;                                                                                \
      }                                                                                                 \
    }                                                                                                   \
    CHECK_MESSAGE(foundNan, "Did not find NaN");                                                        \
  }

TEST_SUITE("Floating Point GEN") {
  TEST_CASE("double") {
    FP_CASE(double, 0, 1000);
    FP_CASE(double, -10000, -1000);
    FP_CASE(double, -1000, 0);
    FP_CASE(double, 1000, 10000);
    FP_CASE(double, 5, 5);
    INF_CASE(double);
    NAN_CASE(double);
  }

  TEST_CASE("float") {
    FP_CASE(float, 0, 1000);
    FP_CASE(float, -10000, -1000);
    FP_CASE(float, -1000, 0);
    FP_CASE(float, 1000, 10000);
    FP_CASE(float, 5, 5);
    INF_CASE(float);
    NAN_CASE(float);
  }

  TEST_CASE("long double") {
    FP_CASE(long double, 0, 1000);
    FP_CASE(long double, -10000, -1000);
    FP_CASE(long double, -1000, 0);
    FP_CASE(long double, 1000, 10000);
    FP_CASE(long double, 5, 5);
    INF_CASE(long double);
    NAN_CASE(long double);
  }
}
