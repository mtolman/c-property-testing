#include <doctest/doctest.h>
#include <prop_test.h>

#define INT_CASE(Type, Min, Max) \
  SUBCASE("Min: " # Min ", Max: " # Max) { \
  auto gen = prop_test::gen::integer::ints<Type>({.min=Min, .max=Max}); \
  for (size_t i = 0; i < 100; ++i) { \
    const std::integral auto v = gen.create(i); \
    CHECK_GE(v, Min); \
    CHECK_LE(v, Max); \
  } \
}

TEST_SUITE("Integer GEN") {
  TEST_CASE("int64") {
    INT_CASE(int64_t, 0, 1000);
    INT_CASE(int64_t, -10000, -1000);
    INT_CASE(int64_t, -1000, 0);
    INT_CASE(int64_t, 1000, 10000);
    INT_CASE(int64_t, 5, 5);
  }

  TEST_CASE("int32") {
    INT_CASE(int32_t, 0, 1000);
    INT_CASE(int32_t, -10000, -1000);
    INT_CASE(int32_t, -1000, 0);
    INT_CASE(int32_t, 1000, 10000);
    INT_CASE(int32_t, 5, 5);
  }

  TEST_CASE("int16") {
    INT_CASE(int16_t, 0, 1000);
    INT_CASE(int16_t, -10000, -1000);
    INT_CASE(int16_t, -1000, 0);
    INT_CASE(int16_t, 1000, 10000);
    INT_CASE(int16_t, 5, 5);
  }

  TEST_CASE("int8") {
    INT_CASE(int8_t, 0, 100);
    INT_CASE(int8_t, -100, -10);
    INT_CASE(int8_t, -100, 0);
    INT_CASE(int8_t, 10, 100);
    INT_CASE(int8_t, 5, 5);
  }

  TEST_CASE("uint64") {
    INT_CASE(uint64_t, 0, 1000);
    INT_CASE(uint64_t, 1000, 10000);
    INT_CASE(uint64_t, 5, 5);
  }

  TEST_CASE("uint32") {
    INT_CASE(uint32_t, 0, 1000);
    INT_CASE(uint32_t, 1000, 10000);
    INT_CASE(uint32_t, 5, 5);
  }

  TEST_CASE("uint16") {
    INT_CASE(uint16_t, 0, 1000);
    INT_CASE(uint16_t, 1000, 10000);
    INT_CASE(uint16_t, 5, 5);
  }

  TEST_CASE("uint8") {
    INT_CASE(uint8_t, 0, 100);
    INT_CASE(uint8_t, 10, 100);
    INT_CASE(uint8_t, 5, 5);
  }
}
